<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
$return = $_SERVER['REQUEST_URI'];
$return = str_replace('//', '/', $return);
$user = JFactory::getUser();
if (!class_exists('InboxHelpersInbox')) {
    include_once JPATH_COMPONENT . '/com_inbox/helpers/inbox.php';
}
$HelpersInbox = new InboxHelpersInbox();

function getEmployeeId($userId) {
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query
            ->select('id')
            ->from($db->quoteName('#__employees_employee'))
            ->where('user_id = ' . $userId);
    $db->setQuery($query);
    $objResult = $db->loadResult();
    if ($objResult) {
        return $objResult;
    } else {
        return 0;
    }
}

// The menu class is deprecated. Use nav instead
?>
<ul class="nav menutop">
    <li class="nav-item menubg1">
        <a class="nav-link active" href="<?php echo JRoute::_('index.php?option=com_employees&view=employeeform&id=' . getEmployeeId($user->id)); ?>">
            <img src="images/icon-user.png" alt=""/>
            <br/>
            <?php echo JText::_('MOD_USER_MY_PROFILE'); ?>
        </a>
    </li>
    <li class="nav-item menubg2">
        <a class="nav-link" href="<?php echo $user->id > 0 ? JRoute::_('index.php?option=com_inbox&view=messages') : JRoute::_('index.php?option=com_users&view=login'); ?>">
            <img src="images/icon-inbox.png" alt=""/><br/>
            <?php echo JText::_('MOD_USER_INBOX'); ?>
            <?php
            if ($user->id > 0) {
                ?>
                <span class="tv-badge"><?php echo $HelpersInbox->getTotalIsread() ?></span>
            <?php } ?>
        </a>
    </li>
    <li class="nav-item menubg3">
        <?php
        if ($user->id > 0) {
            ?>
            <a class="nav-link" href="#" onclick="document.getElementById('login-form').submit();">
                <img src="images/icon-logout.png" alt=""/><br/>
                <?php echo JText::_('MOD_USER_LOGOUT'); ?>
            </a>
            <form action="<?php echo JRoute::_('index.php'); ?>" method="post" id="login-form" class="form-vertical">
                <div class="logout-button">
                    <input type="hidden" name="option" value="com_users" />
                    <input type="hidden" name="task" value="user.logout" />
                    <input type="hidden" name="return" value="<?php echo base64_encode($return); ?>" />
                    <?php echo JHtml::_('form.token'); ?>
                </div>
            </form>
        <?php } else {
            ?>
            <a class="nav-link" href="<?php echo JRoute::_('index.php?option=com_users&view=login'); ?>">
                <img src="images/icon-signin.png" alt=""/><br/>
                <?php echo JText::_('MOD_USER_LOGIN'); ?>
            </a>
            <?php
        }
        ?>
    </li>
</ul>