<?php
/*
 * Created on : Jan 29, 2018, 11:08:20 AM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */
?>
<p>The latest business and financial headlines from leading publishers to help manage your finances.</p>
<div class="color2398db bold  font16">
    <ul>
        <?php
        if (!empty($files)) {
            foreach ($files as $v) {
// General
                $linkDownloadB = '';
                $linkDownloadE = '';
                if ((int) $v->confirm_license > 0) {
                    $linkDownloadB = '<a class="" href="' . JRoute::_(PhocaDownloadRoute::getFileRoute($v->id, $v->catid, $v->alias, $v->categoryalias, $v->sectionid)) . '" >'; // we need pagination to go back			
                    $linkDownloadE = '</a>';
                } else {
                    if ($v->link_external != '' && $v->directlink == 1) {
                        $linkDownloadB = '<a class="" href="' . $v->link_external . '" target="' . $this->t['download_external_link'] . '" >';
                        $linkDownloadE = '</a>';
                    } else {
                        //echo $v->id.'catid='. $v->catid.'alias='. $v->alias.'categoryalias='. $v->categoryalias.'sectionid='. $v->sectionid;
                        $linkDownloadB = '<a class="" href="' . JRoute::_(PhocaDownloadRoute::getFileRoute($v->id, $v->catid, $v->alias, $v->categoryalias, $v->sectionid, 'download')) . '" >';
                        $linkDownloadE = '</a>';
                    }
                }
                $cBtnSuccess = 'color2398db';
                $pdButtonDownload = str_replace('class=""', 'class="' . $cBtnSuccess . '"', $linkDownloadB) . JText::_('MOD_PHOCADOWNLOAD_FILES_DOWNLOAD') . '<span class="icon-download">&nbsp;</span>' . $linkDownloadE;
                ?>
                <li>
                    <div class="row">
                        <div class="col-md-3">
                            <?php echo $v->title; ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $pdButtonDownload; ?>
                        </div>
                    </div>
                </li>
                <?php
            }
        }
        ?>
    </ul>
</div>