<?php
/*
 * Created on : Jan 24, 2018, 9:30:18 AM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */
?>
<div class="form-group clearfix"></div>
<div class="box-right bgwhite announcement <?php echo $moduleclass_sfx; ?>">
    <div class="box-title brnone">
        <h1><?php echo $module->title; ?></h1>
    </div>
    <div class="box-content">
        <?php
        $c = count($list);
        $r = ceil($c / 3);
        $r = 5;
        $i = 1;
        if (count($c) > 0) {
            foreach ($list as $item) {
                ?>
                <div class="row">
                    <div class="col-1 text-right">
                        <?php echo $i; ?>
                    </div>    
                    <div class="col-10">
                        <a href="<?php echo $item->link; ?>">
                            <?php echo $limitchar > 0 ? ModArticlesTvLatestHelper::characterLimit($item->title, $limitchar) : $item->title; ?>
                        </a>
                    </div>     
                </div>
                <?php
                $i++;
            }
        }
        ?>
        <a class="go-announcement" href="<?php echo JRoute::_(ContentHelperRoute::getCategoryRoute($params->get('catid', 8))); ?>">&nbsp;</a>
    </div>
</div>
