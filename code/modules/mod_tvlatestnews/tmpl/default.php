<?php
/*
 * Created on : Jan 24, 2018, 9:30:18 AM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */
$app = JFactory::getApplication();

// Getting params from template
$params = $app->getTemplate(true)->params;
$noimagearticle = $params->get('noimagearticle', '');
?>
<div class="box-slide bgwhite <?php echo $moduleclass_sfx; ?>">
    <div class="box-title">
        <h1><img src="images/icon-latest.png" alt=""/> <span><?php echo $module->title; ?></span></h1>
    </div>
    <div class="box-content pl20">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <ul class="nav flex-column">
                    <?php
                    $c = count($list);
                    $r = ceil($c / 3);
                    $r = 5;
                    $i = 1;
                    if (count($c) > 0) {
                        foreach ($list as $item) {
                            $image = json_decode($item->images);
                            ?>
                            <li>
                                <div class="row">
                                    <div class="col-3">
                                        <?php if ($image->image_intro != '') : ?>
                                            <a href="<?php echo $item->link; ?>">
                                                <img src="<?php echo JUri::root() . $image->image_intro . ''; ?>"/>
                                            </a> 
                                            <?php
                                        else:
                                            if (file_exists(JPATH_ROOT . '/' . $noimagearticle)):
                                                ?>
                                                <a href="<?php echo $item->link; ?>">
                                                    <img src="<?php echo JUri::root() . $noimagearticle . ''; ?>"/>
                                                </a> 
                                                <?php
                                            else:
                                                ?>
                                                <a href="<?php echo $item->link; ?>">
                                                    <img src="<?php echo JUri::root() . 'components/com_events/assets/images/noimageevent.png'; ?>"/>
                                                </a> 
                                            <?php
                                            endif;
                                        endif;
            ?>
                                    </div>    
                                    <div class="col-9">
                                        <h2><a href="<?php echo $item->link; ?>"><?php echo $item->title ?></a></h2>
                                        <p class="text-fade">
                                            Posted on <?php echo ModArticlesTvLatestHelper::formatDate($item->created, 'd/m/Y'); ?>
                                        </p>
                                    </div>    
                                </div>
                            </li>
                            <?php
                            if ($i % $r == 0 && $i < $c) {
                                ?>
                            </ul>
                        </div>
                        <div class="item">
                            <ul class="nav flex-column">
                                <?php
                            }
                            $i++;
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
        <a class="go-laltest" href="<?php echo $link_latestnews; ?>">&nbsp;</a>
        <!--<a class="go-laltest" href="<?php //echo JRoute::_(ContentHelperRoute::getCategoryRoute($params->get('catid', 8)));     ?>">&nbsp;</a>-->
    </div>
</div>