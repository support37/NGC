<?php

if (!defined('FILE_CACHE_DIRECTORY'))
    define('FILE_CACHE_DIRECTORY', dirname(dirname(dirname(__FILE__))) . '/cache');  // Directory where images are cached. Left blank it will use the system temporary directory (which is better for security)

