
/*
 * Created on : Jan 23, 2018, 9:19:30 PM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */


/*
 * Created on : Jan 23, 2018, 9:03:25 PM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */
jQuery(document).ready(function ($) {
    $('.list-events .tv-assign, .event-detail .tv-assign').click(function (e) {
        var $me = $(this);
        var $url = $(this).attr('action');
        if ($.trim($url) == '') {
            alert('This event has been assigned!');
            return true;
        }
        $(".body").LoadingOverlay("show");
        $.ajax({
            url: $url,
            type: 'post',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $(".body").LoadingOverlay("hide");
                if (data.result == true) {
                    $me.addClass('disabled').attr('action', '');
                    alert(data.html);
                } else {
                    alert(data.html);
                }
            }
        })
    })
    $('.filter-monthto > li').click(function (e) {
        $('.filter-monthto > li').removeClass('active');
        $(this).addClass('active');
    })
    $('.filter-yearto > li').click(function (e) {
        $('.filter-yearto > li').removeClass('active');
        $(this).addClass('active');
    })
    
    $('.filter-monthfrom > li').click(function (e) {
        $('.filter-monthfrom > li').removeClass('active');
        $(this).addClass('active');
    })
    $('.filter-yearfrom > li').click(function (e) {
        $('.filter-yearfrom > li').removeClass('active');
        $(this).addClass('active');
    })
});