<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Events
 * @author     Thang Tran <trantrongthang1207@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_events');

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_events')) {
    $canEdit = JFactory::getUser()->id == $this->item->created_by;
}

$user = JFactory::getUser();
$userId = $user->get('id');
$HelpersEvents = new EventsHelpersEvents();

$document = JFactory::getDocument();
$document->addScript(JUri::root() . 'components/com_events/assets/js/loadingoverlay.js');
$document->addScript(JUri::root() . 'components/com_events/assets/js/com_events.js');
$app = JFactory::getApplication();
$params = $app->getTemplate(true)->params;
$noimageevent = $params->get('noimageevent', '');
?>



<?php if ($canEdit && $this->item->checked_out == 0): ?>

    <a class="btn" href="<?php echo JRoute::_('index.php?option=com_events&task=event.edit&id=' . $this->item->id); ?>"><?php echo JText::_("COM_EVENTS_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete', 'com_events.event.' . $this->item->id)) : ?>

    <a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
        <?php echo JText::_("COM_EVENTS_DELETE_ITEM"); ?>
    </a>

    <div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3><?php echo JText::_('COM_EVENTS_DELETE_ITEM'); ?></h3>
        </div>
        <div class="modal-body">
            <p><?php echo JText::sprintf('COM_EVENTS_DELETE_CONFIRM', $this->item->id); ?></p>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal">Close</button>
            <a href="<?php echo JRoute::_('index.php?option=com_events&task=event.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
                <?php echo JText::_('COM_EVENTS_DELETE_ITEM'); ?>
            </a>
        </div>
    </div>

<?php endif; ?>

<div class="bgwhite event-detail">
    <div class="tv-caption">
        <h5 class=""><?php echo $this->item->title; ?></h5>
    </div>
    <div class="tv-content">
        <div class="row">
            <div class="col-md-6">
                <?php if (!empty($this->item->images)) : ?>
                    <?php $imagesFiles = array(); ?>
                    <?php foreach ((array) $this->item->images as $fileSingle) : ?>
                        <?php if (!is_array($fileSingle)) : ?>
                            <img src="<?php echo JRoute::_(JUri::root() . 'uploads/' . $fileSingle, false); ?>"/>
                        <?php else: ?>
                            <img src="<?php echo JRoute::_(JUri::root() . 'uploads/noimageevent.png', false); ?>"/>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php
                else:
                    if (file_exists(JPATH_ROOT . '/' . $noimageevent)):
                        ?>
                        <img src="<?php echo 'modules/mod_tvlatestnews/timthumb.php?src=' . JUri::root() . $noimageevent . '&w=430&h=305&q=100'; ?>"/>
                        <?php
                    endif;
                    ?>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <p>
                    <span class="minw35"><img src="images/icon-calander.png" alt=""/></span><?php echo $this->item->date_start; ?>
                </p>
                <p style="text-transform: lowercase">
                    <span class="minw35"><img src="images/icon-clock2.png" alt=""/></span><?php echo $this->item->time_start; ?>
                </p>
                <?php if ($this->item->address) { ?>
                    <p>
                        <span class="minw35"><img src="images/icon-address.png" alt=""/></span><?php echo $this->item->address; ?>
                    </p>
                <?php } ?>
                <p>
                    <?php
                    if ($userId > 0) {
                        if ($HelpersEvents->isAssign($userId, $this->item->id)) {
                            ?>
                            <button action="" class="btn btn-primary tv-btn tv-assign disabled"><?php echo JText::_('COM_EVENTS_SAVE_TO_CALENDER'); ?></button>
                            <?php
                        } else {
                            ?>
                            <button action="<?php echo JRoute::_('index.php?option=com_events&view=event&task=event.assign&id=' . (int) $this->item->id . '&userid=' . $userId); ?>" class="btn btn-primary tv-btn tv-assign" <?php echo $userId > 0 ? '' : 'disabled=""'; ?>><?php echo JText::_('COM_EVENTS_SAVE_TO_CALENDER'); ?></button>
                        <?php } ?>
                    <?php } else { ?>
                        <a href="<?php echo JRoute::_('index.php?option=com_users&view=login'); ?>" class="btn btn-primary tv-btn"><?php echo JText::_('COM_EVENTS_SAVE_TO_CALENDER'); ?></a>
                    <?php } ?>
                </p>
            </div>
        </div>
        <div class="form-group clearfix">&nbsp;</div>
        <div>
            <?php echo $this->item->description; ?>
        </div>
    </div>
</div>