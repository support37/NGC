<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Events
 * @author     Thang Tran <trantrongthang1207@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_events');

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_events'))
{
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_TITLE'); ?></th>
			<td><?php echo $this->item->title; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_ALIAS'); ?></th>
			<td><?php echo $this->item->alias; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_DATE'); ?></th>
			<td><?php echo $this->item->date; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_TIME'); ?></th>
			<td><?php echo $this->item->time; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_DATE_END'); ?></th>
			<td><?php echo $this->item->date_end; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_ADDRESS'); ?></th>
			<td><?php echo $this->item->address; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_DESCRIPTION'); ?></th>
			<td><?php echo $this->item->description; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_IMAGES'); ?></th>
			<td>
			<?php
			foreach ((array) $this->item->images as $singleFile) : 
				if (!is_array($singleFile)) : 
					$uploadPath = 'uploads' . DIRECTORY_SEPARATOR . $singleFile;
					 echo '<a href="' . JRoute::_(JUri::root() . $uploadPath, false) . '" target="_blank">' . $singleFile . '</a> ';
				endif;
			endforeach;
		?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_REPEAT_METHOD'); ?></th>
			<td><?php echo $this->item->repeat_method; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_REPEAT'); ?></th>
			<td><?php echo $this->item->repeat; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_WEEK'); ?></th>
			<td><?php echo $this->item->week; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_MONTH'); ?></th>
			<td><?php echo $this->item->month; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_MONTH_TYPE'); ?></th>
			<td><?php echo $this->item->month_type; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_MONTHLY_LIST'); ?></th>
			<td><?php echo $this->item->monthly_list; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_MONTH_WEEK'); ?></th>
			<td><?php echo $this->item->month_week; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_YEAR_MONTH'); ?></th>
			<td><?php echo $this->item->year_month; ?></td>
		</tr>

	</table>

</div>

<?php if($canEdit && $this->item->checked_out == 0): ?>

	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_events&task=event.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_EVENTS_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete','com_events.event.'.$this->item->id)) : ?>

	<a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
		<?php echo JText::_("COM_EVENTS_DELETE_ITEM"); ?>
	</a>

	<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo JText::_('COM_EVENTS_DELETE_ITEM'); ?></h3>
		</div>
		<div class="modal-body">
			<p><?php echo JText::sprintf('COM_EVENTS_DELETE_CONFIRM', $this->item->id); ?></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<a href="<?php echo JRoute::_('index.php?option=com_events&task=event.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
				<?php echo JText::_('COM_EVENTS_DELETE_ITEM'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>