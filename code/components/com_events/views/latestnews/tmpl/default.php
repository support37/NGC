<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Events
 * @author     Thang Tran <trantrongthang1207@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JLoader::register('ContentHelperRoute', JPATH_SITE . '/components/com_content/helpers/route.php');

JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_content/models', 'ContentModel');
//str_replace('Itemid', 'Item', 
$HelpersEvents = new EventsHelpersEvents();

$app = JFactory::getApplication();

// Getting params from template
$params = $app->getTemplate(true)->params;
$noimagearticle = $params->get('noimagearticle', '');


$document = JFactory::getDocument();
$document->addScript(JUri::root() . 'components/com_events/assets/js/com_events.js');
$document->addStyleSheet(JUri::root() . 'components/com_events/assets/css/com_events.css');
$monthed = $this->state->get("filter.lnmonth", 0);
$yeared = $this->state->get("filter.lnyear", 0);
$year = date('Y');
?>
<?php //echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__));   ?>
<div class="latest-news">
    <div class="row">
        <div class="col-9 padright0">       
            <div class="tv-title">
                <h5><?php echo JText::_('COM_EVENTS_LATEST_NEWS') ?></h5>
            </div>
        </div>
        <div class="col-3">
            <form action="<?php echo htmlspecialchars(JUri::getInstance()->toString()); ?>" method="post"
                  name="adminForm" id="adminForm">
                <ul class="nav tv-tab menu filter-events">
                    <li class="nav-item item-140 level-1 deeper parent">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php echo JText::_('COM_EVENTS_FILTER'); ?>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <label><?php echo JText::_('COM_EVENTS_SELECT_BY_MONTH'); ?></label>
                                <ul class="nav tv-tab menu filter-month">
                                    <li class="<?php echo ($monthed == 0) ? 'active' : ''; ?>">
                                        <a class="nav-link">
                                            <label for="month-0"><?php echo JText::_('COM_EVENTS_ALL'); ?></label>
                                            <!--<input class="hide" id="month-0" type="radio" name="filter[month]" onclick="this.form.submit();" value="0"/>-->
                                            <input class="hide" id="month-0" type="radio" name="filter[lnmonth]" value="0" <?php echo ($monthed == 0) ? ' checked="" ' : ''; ?>/>
                                        </a>
                                    </li>
                                    <?php for ($i = 1; $i < 13; $i++) {
                                        ?>
                                        <li class="<?php echo ($monthed == $i) ? 'active' : ''; ?>">
                                            <a class="nav-link">
                                                <label for="month-<?php echo $i ?>"><?php echo $i ?></label> 
                                                <input class="hide" id="month-<?php echo $i ?>" type="radio" name="filter[lnmonth]" value="<?php echo $i ?>" <?php echo ($monthed == $i) ? ' checked="" ' : ''; ?>/>
                                            </a>
                                        </li>
                                    <?php }
                                    ?>
                                </ul>
                                <label><?php echo JText::_('COM_EVENTS_SELECT_BY_YEAR'); ?></label>
                                <ul class="nav tv-tab menu filter-year">
                                    <li class="<?php echo ($yeared == 0) ? 'active' : ''; ?>">
                                        <a class="nav-link">
                                            <label for="year-0"><?php echo JText::_('COM_EVENTS_ALL'); ?></label> 
                                            <input class="hide" id="year-0" type="radio" name="filter[lnyear]" value="0" <?php echo ($yeared == $i) ? ' checked="" ' : ''; ?>/>
                                        </a>
                                    </li>
                                    <?php for ($i = $year; $i >= ($year - 6); $i--) {
                                        ?>
                                        <li class="<?php echo ($yeared == $i) ? 'active' : ''; ?>">
                                            <a class="nav-link">
                                                <label for="year-<?php echo $i ?>"><?php echo $i ?></label> 
                                                <input class="hide" id="year-<?php echo $i ?>" type="radio" name="filter[lnyear]" value="<?php echo $i ?>" <?php echo ($yeared == $i) ? ' checked="" ' : ''; ?>/>
                                            </a>
                                        </li>
                                    <?php }
                                    ?>                       
                                </ul>
                                <button class="btn-search" onclick="this.form.submit();" >
                                    <label><?php echo JText::_('COM_EVENTS_SEARCH_FILTER_SUBMIT'); ?></label>
                                </button>
                            </div>
                        </div>
                    </li>
                </ul>

                <input type="hidden" name="task" value="">
                <input type="hidden" name="boxchecked" value="0">
                <?php echo JHtml::_('form.token'); ?>
            </form>
        </div>
    </div>
    <?php
    foreach ($this->items as $i => $item) :
        $image = json_decode($item->images);
        $item->slug = $item->id . ':' . $item->alias;
        $item->link = JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catid, $item->language));
        $link = explode('Itemid', $item->link)[0] . '?Itemid=0';
        $item->link = $link;
        ?>
        <div class="media bgwhite list-events">
            <?php if ($image->image_intro != '') : ?>
                <a href="<?php echo $item->link; ?>">
                    <img src="<?php echo JUri::root() . $image->image_intro; ?>"/>
                </a> 
                <?php
            else:
                if (file_exists(JPATH_ROOT . '/' . $noimagearticle)):
                    ?>
                    <a href="<?php echo $item->link; ?>">
                        <img src="<?php echo JUri::root() . $noimagearticle; ?>"/>
                    </a> 
                    <?php
                else:
                    ?>
                    <a href="<?php echo $item->link; ?>">
                        <img src="<?php echo JUri::root() . 'components/com_events/assets/images/noimageevent.png'; ?>"/>
                    </a> 
                <?php
                endif;
            endif;
            ?>
            <div class="media-body">
                <h5 class=""><a href="<?php echo $item->link; ?>"><?php echo $this->escape($item->title); ?></a></h5>
                <div><?php echo $HelpersEvents->wordLimit($item->introtext, 50); ?></div>
                <div class="text-fade">
                    <span>
                        <img src="images/icon-calender.png" alt=""/><?php echo JText::_('COM_EVENTS_POSTED_ON'); ?> 
                        <?php echo JHtml::_('date', $item->created, JText::_('d/m/Y')); ?>
                    </span>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    <?php echo $this->pagination->getListFooter(); ?>
</div>



