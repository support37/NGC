<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Events
 * @author     Thang Tran <trantrongthang1207@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

$user = JFactory::getUser();
$userId = $user->get('id');
$HelpersEvents = new EventsHelpersEvents();

$document = JFactory::getDocument();
$document->addScript(JUri::root() . 'components/com_events/assets/js/loadingoverlay.js');
$document->addScript(JUri::root() . 'components/com_events/assets/js/com_events.js');
$document->addStyleSheet(JUri::root() . 'components/com_events/assets/css/com_events.css');

$params = JComponentHelper::getParams('com_events');
$wordlimit = $params->get('wordlimit', 25);

$app = JFactory::getApplication();
$params = $app->getTemplate(true)->params;
$noimageevent = $params->get('noimageevent', '');

$monthedto = $this->state->get("filter.monthto", 0);
$yearedto = $this->state->get("filter.yearto", 0);

$monthedfrom = $this->state->get("filter.monthfrom", 0);
$yearedfrom = $this->state->get("filter.yearfrom", 0);
$year = date('Y');
?>
<?php //echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__));      ?>
<div class="row">
    <div class="col-9 padright0">
        <div class="tv-title">
            <h5><?php echo JText::_('COM_EVENTS_TITLE_EVENTS'); ?></h5>
        </div>
    </div>
    <div class="col-3">
        <form action="<?php echo htmlspecialchars(JUri::getInstance()->toString()); ?>" method="post"
              name="adminForm" id="adminForm">
                  <?php echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__)); ?>

            <input type="hidden" name="task" value="">
            <input type="hidden" name="boxchecked" value="0">
            <input type="hidden" id="js-stools-field-order" class="js-stools-field-order" name="list[fullordering]" value="null ASC">

            <?php echo JHtml::_('form.token'); ?>
        </form>
    </div>
</div>
<div class="clearfix"></div>
<?php foreach ($this->items as $i => $item) : ?>
    <div class="media bgwhite list-events">
        <?php if (!empty($item->images)) : ?>
            <?php $imagesFiles = array(); ?>
            <?php foreach ((array) $item->images as $fileSingle) : ?>
                <?php if (!is_array($fileSingle)) : ?>
                    <a href="<?php echo JRoute::_('index.php?option=com_events&view=event&id=' . (int) $item->id); ?>">
                        <img src="<?php echo JRoute::_(JUri::root() . 'uploads/' . $fileSingle, false); ?>" width="309"/>
                    </a> 
                <?php else: ?>
                    <a href="<?php echo JRoute::_('index.php?option=com_events&view=event&id=' . (int) $item->id); ?>">
                        <img src="<?php echo JRoute::_(JUri::root() . 'uploads/noimageevent.png', false); ?>" width="309"/>
                    </a> 
                <?php endif; ?>
            <?php endforeach; ?>
            <?php
        else:
            if (file_exists(JPATH_ROOT . '/' . $noimageevent)):
                ?>
                <a href="<?php echo JRoute::_('index.php?option=com_events&view=event&id=' . (int) $item->id); ?>">
                    <img src="<?php echo JUri::root() . $noimageevent . ''; ?>"/>
                </a> 
                <?php
            else:
                ?>
                <a href="<?php echo JRoute::_('index.php?option=com_events&view=event&id=' . (int) $item->id); ?>">
                    <img src="<?php echo JUri::root() . 'components/com_events/assets/images/noimageevent.png'; ?>"/>
                </a> 
            <?php
            endif;
        endif;
        ?>
        <div class="media-body">
            <h5 class=""><a href="<?php echo JRoute::_('index.php?option=com_events&view=event&id=' . (int) $item->id); ?>" class=""><?php echo $this->escape($item->title); ?></a></a></h5>
            <div>
                <?php echo $HelpersEvents->wordLimit($item->description, $wordlimit); ?>
            </div>
            <div class="text-fade">
                <span>
                    <img src="images/icon-calender.png" alt=""/> <?php echo JHtml::_('date', $item->date_start, 'd/m/Y'); ?></a>
                    <?php
                    if (!$item->time_start) {
                        $from[0] = "";
                        $from[1] = "";
                        $to[0] = "";
                        $to[1] = "";
                    } else {
                        $from_to = explode("-", $item->time_start);
                        $from = explode(":", $from_to[0]);
                        if (isset($from_to[1]))
                            $to = explode(":", $from_to[1]);
                        else {
                            $to[0] = "";
                            $to[1] = "";
                        }
                    }
                    ?> 
                    <img src="images/icon-clock.png" alt=""/><span class="text-lowercase"><?php echo $from_to[0]; ?></span>
                </span>
                <?php
                if ($userId > 0) {
                    if ($HelpersEvents->isAssign($userId, $item->id)) {
                        ?>
                        <button action="" class="btn btn-primary tv-btn pull-right tv-assign disabled"><?php echo JText::_('COM_EVENTS_SAVE_TO_CALENDER'); ?></button>
                        <?php
                    } else {
                        ?>
                        <button action="<?php echo JRoute::_('index.php?option=com_events&view=event&task=event.assign&id=' . (int) $item->id . '&userid=' . $userId); ?>" class="btn btn-primary tv-btn pull-right tv-assign" <?php echo $userId > 0 ? '' : 'disabled=""'; ?>><?php echo JText::_('COM_EVENTS_SAVE_TO_CALENDER'); ?></button>
                    <?php } ?>
                <?php } else { ?>
                    <a href="<?php echo JRoute::_('index.php?option=com_users&view=login'); ?>" class="btn btn-primary tv-btn pull-right"><?php echo JText::_('COM_EVENTS_SAVE_TO_CALENDER'); ?></a>
                <?php } ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>

<?php echo $this->pagination->getListFooter(); ?>

