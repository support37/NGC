<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Employees
 * @author     Thang Tran <thang.testdev@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$canCreate = $user->authorise('core.create', 'com_employees') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'employeeform.xml');
$canEdit = $user->authorise('core.edit', 'com_employees') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'employeeform.xml');
$canCheckin = $user->authorise('core.manage', 'com_employees');
$canChange = $user->authorise('core.edit.state', 'com_employees');
$canDelete = $user->authorise('core.delete', 'com_employees');

$document = JFactory::getDocument();
$document->addScript(JUri::base() . 'components/com_employees/assets/js/jquery.mask.min.js');
$document->addScript(JUri::base() . 'components/com_employees/assets/js/com_employees.js');
$document->addStyleSheet(JUri::root() . 'components/com_employees/assets/css/com_employees.css');

$str = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z';
$arrStr = explode(',', $str);

$chared = $this->state->get("filter.char", '');
$search = $this->state->get("filter.search", '');
$type = $this->state->get("filter.type", '');
$HelpersEmployees = new EmployeesHelpersEmployees();
$getDepartment = $HelpersEmployees->getDepartment();
?>
<?php //echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__));                    ?>
<div class="bgwhite">
    <form action="<?php echo htmlspecialchars(JUri::getInstance()->toString()); ?>" method="post"
          name="adminForm2" id="adminForm2">
        <div class="filter-row">
            <ul class="nav tv-tab menu filter-type">
                <li class="nav-item marginright">
                    <input type="text" name="filter[search]" class="form-control" value="<?php echo $search ?>"/>
                </li>
                <li class="nav-item parent">
                    <div class="btn-group">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php echo JText::_('COM_EMPLOYEES_BY_NAME'); ?>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <ul class="nav flex-column">
                                <li class="nav-item borderbuttom <?php echo ($type == 'by_name' || $type == '') ? ' active ' : ''; ?>">
                                    <a class="nav-link">
                                        <label for="type-0"><?php echo JText::_('COM_EMPLOYEES_BY_NAME'); ?></label> 
                                        <input class="hide" id="type-0" type="radio" name="filter[type]" value="by_name" <?php echo ($type == 'by_name') ? ' checked="" ' : ''; ?>/>
                                    </a>
                                </li>
                                <li class="nav-item borderbuttom">
                                    <a class="nav-link">
                                        <label for="type-0"><?php echo JText::_('COM_EMPLOYEES_BY_DEPARTMENT'); ?></label> 
                                        <input class="hide" id="type-0" type="radio" name="filter[type]" value="by_name" <?php echo ($type == 'by_name') ? ' checked="" ' : ''; ?>/>
                                    </a>
                                </li>
                                <?php
                                $i = 0;
                                foreach ($getDepartment as $key => $value) {
                                    $i++;
                                    if (trim($value->department) != '') {
                                        ?>
                                        <li class="nav-item <?php echo (trim($type) == trim($value->department)) ? ' active ' : ''; ?>">
                                            <a class="nav-link">
                                                <label for="type-<?php echo $i ?>"><?php echo $value->department; ?></label> 
                                                <input class="hide" id="type-<?php echo $i ?>" type="radio" name="filter[type]" value="<?php echo $value->department; ?>" <?php echo ($type == $arrStr[$i]) ? ' checked="" ' : ''; ?>/>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <button class="btn-search btn menubg2" onclick="this.form.submit();">
                        <label><?php echo JText::_('COM_EMPLOYEES_SEARCH_FILTER_SUBMIT'); ?></label>
                    </button>
                </li>
            </ul>
        </div>
    </form>
    <form action="<?php echo htmlspecialchars(JUri::getInstance()->toString()); ?>" method="post"
          name="adminForm" id="adminForm">
        <div class="filter-row">
            <ul class="nav tv-tab menu filter-char">
                <?php for ($i = 0; $i < count($arrStr); $i++) {
                    ?>
                    <li class="<?php echo ($chared == $arrStr[$i]) ? 'active' : ''; ?>">
                        <a class="nav-link">
                            <label for="month-<?php echo $i ?>"><?php echo $arrStr[$i] ?></label> 
                            <input onclick="this.form.submit();" class="hide" id="month-<?php echo $i ?>" type="radio" name="filter[char]" value="<?php echo $arrStr[$i] ?>" <?php echo ($chared == $arrStr[$i]) ? ' checked="" ' : ''; ?>/>
                        </a>
                    </li>
                <?php }
                ?>
            </ul>
        </div>

        <table class="table table-bordered" id="employeeList">
            <thead>
                <tr>
                    <th class=''>
                        <?php echo JHtml::_('grid.sort', 'COM_EMPLOYEES_EMPLOYEES_NAME', 'a.first_name', $listDirn, $listOrder); ?>
                    </th>

                    <th class=''>
                        <?php echo JHtml::_('grid.sort', 'COM_EMPLOYEES_EMPLOYEES_TITLE', 'a.title', $listDirn, $listOrder); ?>
                    </th>
                    <th class=''>
                        <?php echo JHtml::_('grid.sort', 'COM_EMPLOYEES_EMPLOYEES_DEPARTMENT', 'a.department', $listDirn, $listOrder); ?>
                    </th>
                    <th class=''>
                        <?php echo JHtml::_('grid.sort', 'COM_EMPLOYEES_FORM_LBL_EMPLOYEE_REPORT_TO', 'a.deparment_manager', $listDirn, $listOrder); ?>
                    </th>
                    <th class=''>
                        <?php echo JHtml::_('grid.sort', 'COM_EMPLOYEES_EMPLOYEES_EMAIL', 'a.email', $listDirn, $listOrder); ?>
                    </th>
                    <th class=''>
                        <?php echo JHtml::_('grid.sort', 'COM_EMPLOYEES_EMPLOYEES_MOBILE', 'a.mobile', $listDirn, $listOrder); ?>
                    </th>


                    <?php if ($canEdit || $canDelete): ?>
                        <th class="center hide">
                            <?php echo JText::_('COM_EMPLOYEES_EMPLOYEES_ACTIONS'); ?>
                        </th>
                    <?php endif; ?>

                </tr>
            </thead>
            <tbody>
                <?php foreach ($this->items as $i => $item) : ?>
                    <?php $canEdit = $user->authorise('core.edit', 'com_employees'); ?>

                    <?php if (!$canEdit && $user->authorise('core.edit.own', 'com_employees')): ?>
                        <?php $canEdit = JFactory::getUser()->id == $item->created_by; ?>
                    <?php endif; ?>

                    <tr class="row<?php echo $i % 2; ?>">
                        <td class="hide">
                            <?php if (isset($item->checked_out) && $item->checked_out) : ?>
                                <?php echo JHtml::_('jgrid.checkedout', $i, $item->uEditor, $item->checked_out_time, 'employees.', $canCheckin); ?>
                            <?php endif; ?>
                            <a href="<?php echo JRoute::_('index.php?option=com_employees&view=employee&id=' . (int) $item->id); ?>">
                                <?php echo $this->escape($item->first_name); ?> <?php echo $item->last_name; ?></a>
                        </td>
                        <td>
                            <?php echo $this->escape($item->first_name); ?> <?php echo $item->last_name; ?>
                        </td>

                        <td>

                            <?php echo $item->title; ?>
                        </td>
                        <td>

                            <?php echo $item->department; ?>
                        </td>
                        <td>

                            <?php echo $item->deparment_manager; ?>
                        </td>
                        <td>

                            <?php echo $item->email; ?>
                        </td>

                        <td>

                            <?php echo $item->mobile; ?>
                        </td>


                        <?php if ($canEdit || $canDelete): ?>
                            <td class="center hide">
                                <?php if ($canEdit): ?>
                                    <a href="<?php echo JRoute::_('index.php?option=com_employees&task=employeeform.edit&id=' . $item->id, false, 2); ?>" class="btn btn-mini" type="button"><i class="icon-edit" ></i></a>
                                <?php endif; ?>
                                <?php if ($canDelete): ?>
                                    <a href="<?php echo JRoute::_('index.php?option=com_employees&task=employeeform.remove&id=' . $item->id, false, 2); ?>" class="btn btn-mini delete-button" type="button"><i class="icon-trash" ></i></a>
                                <?php endif; ?>
                            </td>
                        <?php endif; ?>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <?php if ($canCreate) : ?>
            <a class="hide" href="<?php echo JRoute::_('index.php?option=com_employees&task=employeeform.edit&id=0', false, 0); ?>"
               class="btn btn-success btn-small"><i
                    class="icon-plus"></i>
                <?php echo JText::_('COM_EMPLOYEES_ADD_ITEM'); ?></a>
            <?php endif; ?>

        <input type="hidden" name="task" value=""/>
        <input type="hidden" name="boxchecked" value="0"/>
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
        <?php echo JHtml::_('form.token'); ?>
    </form>
    <?php echo $this->pagination->getListFooter(); ?>
</div>
<?php if ($canDelete) : ?>
    <script type="text/javascript">

        jQuery(document).ready(function () {
            jQuery('.delete-button').click(deleteItem);
        });

        function deleteItem() {

            if (!confirm("<?php echo JText::_('COM_EMPLOYEES_DELETE_MESSAGE'); ?>")) {
                return false;
            }
        }
    </script>
<?php endif; ?>
