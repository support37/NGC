<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Employees
 * @author     Thang Tran <thang.testdev@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Employees list controller class.
 *
 * @since  1.6
 */
class EmployeesControllerEmployees extends EmployeesController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Employees', $prefix = 'EmployeesModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
