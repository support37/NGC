<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Employees
 * @author     Thang Tran <thang.testdev@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JLoader::register('EmployeesHelper', JPATH_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_employees' . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'employees.php');

/**
 * Class EmployeesFrontendHelper
 *
 * @since  1.6
 */
class EmployeesHelpersEmployees {

    /**
     * Get an instance of the named model
     *
     * @param   string  $name  Model name
     *
     * @return null|object
     */
    public static function getModel($name) {
        $model = null;

        // If the file exists, let's
        if (file_exists(JPATH_SITE . '/components/com_employees/models/' . strtolower($name) . '.php')) {
            require_once JPATH_SITE . '/components/com_employees/models/' . strtolower($name) . '.php';
            $model = JModelLegacy::getInstance($name, 'EmployeesModel');
        }

        return $model;
    }

    /**
     * Gets the files attached to an item
     *
     * @param   int     $pk     The item's id
     *
     * @param   string  $table  The table's name
     *
     * @param   string  $field  The field's name
     *
     * @return  array  The files
     */
    public static function getFiles($pk, $table, $field) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query
                ->select($field)
                ->from($table)
                ->where('id = ' . (int) $pk);

        $db->setQuery($query);

        return explode(',', $db->loadResult());
    }

    /**
     * Gets the edit permission for an user
     *
     * @param   mixed  $item  The item
     *
     * @return  bool
     */
    public static function canUserEdit($item) {
        $permission = false;
        $user = JFactory::getUser();

        if ($user->authorise('core.edit', 'com_employees')) {
            $permission = true;
        } else {
            if (isset($item->created_by)) {
                if ($user->authorise('core.edit.own', 'com_employees') && $item->created_by == $user->id) {
                    $permission = true;
                }
            } else {
                $permission = true;
            }
        }

        return $permission;
    }

    public static function add($post = array()) {
        include_once JPATH_ROOT . '/components/com_employees/models/employee.php';
        $userAssign = JModelLegacy::getInstance('Employee', 'EmployeesModel', array('ignore_request' => true));
        $objTable = $userAssign->getTable();
        if ($objTable->bind($post)) {
            if ($objTable->store()) {
                return $objTable->id;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function update($post = array()) {
        include_once JPATH_ROOT . '/components/com_employees/models/employee.php';
        $userAssign = JModelLegacy::getInstance('Employee', 'EmployeesModel', array('ignore_request' => true));
        $objTable = $userAssign->getTable();
        if (!$objTable->bind($post)) {
            return false;
        }
        if (!$objTable->store()) {
            return false;
        }
        return true;
    }

    public static function delete($id) {
        if ($id > 0) {
            include_once JPATH_ROOT . '/components/com_employees/models/employee.php';
            $userAssign = JModelLegacy::getInstance('Employee', 'EmployeesModel', array('ignore_request' => true));
            $objTable = $userAssign->getTable();
            $objTable->delete($id);
        }
        return true;
    }

    public static function getId($userId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
                ->select('id')
                ->from($db->quoteName('#__employees_employee'))
                ->where('user_id = ' . $userId);
        $db->setQuery($query);
        $objResult = $db->loadResult();
        if ($objResult) {
            return $objResult;
        } else {
            return 0;
        }
    }

    function formatDate($date, $format = 'd/m/Y') {
        if ($date == '0000-00-00' || $date == '' || $date === '0000-00-00 00:00:00') {
            return false;
        }
        if ($format == '' || $format == null) {
            $format = 'Y-m-d H:i:s';
        }
        $date = date_create($date);
        return date_format($date, $format);
    }

    public function getDepartment() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
                ->select('department')
                ->from($db->quoteName('#__employees_employee'))
                ->group('department')
                ->order('department ASC');
        $db->setQuery($query);
        $objResult = $db->loadObjectList();
        if ($objResult) {
            return $objResult;
        } else {
            return 0;
        }
    }

}
