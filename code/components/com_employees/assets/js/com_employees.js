jQuery(document).ready(function ($) {
    $('#joined_since').mask('00/00/0000', {
        placeholder: "__/__/____"
    });
    $('.flex-column > li').click(function (e) {
        $('.flex-column > li').removeClass('active');
        $(this).addClass('active');
        $('#adminForm .dropdown-toggle').html($(this).find('label').text());
        $('#adminForm2 .dropdown-toggle').html($(this).find('label').text());
    })
    $('#adminForm .dropdown-toggle').html($('.flex-column .active label').text());
    $('#adminForm2 .dropdown-toggle').html($('.flex-column .active label').text());
})