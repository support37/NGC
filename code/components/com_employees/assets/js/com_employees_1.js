var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i,
        $maximgdonwload = $('#maximgdonwload').val(),
        $item = 0, $ischeck = 1;

$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = $urlbase + "/components/com_adverts79/upload/",
            uploadButton = $('<button/>')
            .addClass('btn btn-primary')
            .prop('disabled', true)
            .text('Processing...')
            .on('click', function () {
                var $this = $(this),
                        data = $this.data();
                $this
                        .off('click')
                        .text('Abort')
                        .on('click', function () {
                            $this.remove();
                            data.abort();
                        });
                data.submit().always(function () {
                    $this.remove();
                });
            });
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 1024 * 1024,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
        $('#progress').removeClass('hide')
        $item = $('#listfile li').length;
        if ($item < $maximgdonwload) {
            $.each(data.files, function (index, file) {
                if (acceptFileTypes.test(file.name)) {
                    data.context = $('<li class="item"/>').appendTo('#listfile');
                    var node = $('<span/>')
                            .append($('<i class="fa fa-trash delete"></i><input type="hidden" name="jform[advert_images][]" class="iupload"/>').val(file.name));
                    node.appendTo(data.context);
                    data.submit();
                }
            });
        } else {
            if ($('#popupmess').is(':hidden'))
                showmess('Bạn không được phép tải số lương ảnh lơn hơn ' + $maximgdonwload + ' ảnh!');
            return true;
        }
    }).on('fileuploadprocessalways', function (e, data) {
        if (typeof data.context != 'undefined') {
            var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
            if (file.preview) {
                node
                        .prepend('<br>')
                        .prepend(file.preview);
            }
            if (file.error) {
                node
                        .append('<br>')
                        .append($('<span class="text-danger"/>').text(file.error));
                $(data.context).find('input').remove();
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
            }
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
                'width',
                progress + '%'
                );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                        .attr('target', '_blank')
                        .prop('href', file.url);
                $(data.context.children()[index])
                        .wrap(link);
                $(data.context).find('input').val(file.folder + '/' + file.name);
                //$(data.context).find('input').val(file.name);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                        .append('<br>')
                        .append(error);
                $(data.context).find('input').remove();
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            $(data.context).find('input').remove();
        });
    }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled')
            .bind('change', function (e) {
                $('#progress .progress-bar').css(
                        'width',
                        0 + '%'
                        );
            });
    $(document).on('click', '#listfile li a, a.imgitem', function (e) {
        e.preventDefault();
    })
    $(document).on('click', '#listfile li i', function () {
        $(this).closest('li.item').remove();
    })
    $(document).on('click', 'a.imgitem i', function (e) {
        e.preventDefault();
        var $me = $(this),
                $a = $(this).parent(),
                $action = $a.attr('href'),
                $value = [];
        $a.addClass('selected');
        $('a.imgitem').each(function (i) {
            if (!$(this).is('.selected')) {
                $value.push($(this).attr('img'));
            }
        })
        //console.log($value);return ;
        $(".portlet-body").LoadingOverlay("show");
        $.ajax({
            url: $action,
            data: {
                'img': $value,
                'imgdelete': $a.attr('img')
            },
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if (data.return == 1) {
                    $a.remove();
                    $('#jform_advert_images_hidden').val(data.img)
                    $('#system-message-container').html(mess(data.msg));
                    $("html, body").animate({scrollTop: ($("#system-message-container").offset().top - 50) + "px"});
                } else {
                    $('#system-message-container').html(mess(data.msg));
                    $("html, body").animate({scrollTop: ($("#system-message-container").offset().top - 50) + "px"});

                }
                $(".portlet-body").LoadingOverlay("hide");
            }
        })
    })
    function showmess($mess) {
        $('#popupmess .modal-body').html($mess);
        $('#popupmess').modal('toggle');
        if (window.timeoutpopup)
            clearTimeout(window.timeoutpopup)
        window.timeoutpopup = setTimeout(function () {
            $('#popupmess').modal('toggle');
        }, 2000);
    }
});