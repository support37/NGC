<?php 
// namespace administrator\components\com_jchat\views\webrtc;

/**
 * @author Joomla! Extensions Store
 * @package JCHAT::BLACKBOARD::administrator::components::com_jchat
 * @subpackage views
 * @subpackage blackboard
 * @Copyright (C) 2015 Joomla! Extensions Store
 * @license GNU/GPLv2 http://www.gnu.org/licenses/gpl-2.0.html  
 */
defined ( '_JEXEC' ) or die ( 'Restricted access' );

/**
 * View for JS client
 *
 * @package JCHAT::BLACKBOARD::administrator::components::com_jchat
 * @subpackage views
 * @subpackage blackboard
 * @since 2.23
 */
class JChatViewBlackboard extends JChatView {
	/**
	 * Return application/json response to JS client APP
	 * Replace $tpl optional param with $userData contents to inject
	 *        	
	 * @access public
	 * @param string $userData
	 * @return void
	 */
	public function display($userData = null) {
		echo json_encode($userData);  
	}
}