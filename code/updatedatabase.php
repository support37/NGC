<?php

/**
 * @package    Joomla.Site
 *
 * @copyright  Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
/**
 * Define the application's minimum supported PHP version as a constant so it can be referenced within the application.
 */
define('JOOMLA_MINIMUM_PHP', '5.3.10');

if (version_compare(PHP_VERSION, JOOMLA_MINIMUM_PHP, '<')) {
    die('Your host needs to use PHP ' . JOOMLA_MINIMUM_PHP . ' or higher to run this version of Joomla!');
}

// Saves the start time and memory usage.
$startTime = microtime(1);
$startMem = memory_get_usage();

/**
 * Constant that is checked in included files to prevent direct access.
 * define() is used in the installation folder rather than "const" to not error for PHP 5.2 and lower
 */
define('_JEXEC', 1);

if (file_exists(__DIR__ . '/defines.php')) {
    include_once __DIR__ . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', __DIR__);
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_BASE . '/includes/framework.php';

// Set profiler start time and memory usage and mark afterLoad in the profiler.
JDEBUG ? JProfiler::getInstance('Application')->setStart($startTime, $startMem)->mark('afterLoad') : null;
$db = JFactory::getDBO();
$query = 'ALTER TABLE `#__events_event` CHANGE `date` `date_start` DATE NOT NULL;';
$db->setQuery($query);
if ($db->query()) {
    echo 'true:date_start';
} else {
    echo 'false:date_start';
}
$query = 'ALTER TABLE `#__events_event` CHANGE `time` `time_start` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;';
$db->setQuery($query);
if ($db->query()) {
    echo 'true';
} else {
    echo 'false';
}

$query = 'ALTER TABLE `#__events_event` ADD `time_end` VARCHAR(20) NOT NULL AFTER `date_end`;';
$db->setQuery($query);
if ($db->query()) {
    echo 'true';
} else {
    echo 'false';
}