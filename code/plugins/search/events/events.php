<?php

/**
 * @package     Joomla.Plugin
 * @subpackage  Search.events
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JLoader::register('EventsRoute', JPATH_SITE . '/components/com_events/route.php');

/**
 * Categories search plugin.
 *
 * @since  1.6
 */
class PlgSearchEvents extends JPlugin {
    /**
     * Load the language file on instantiation.
     *
     * @var    boolean
     * @since  3.1
     */

    /**
     * Determine areas searchable by this plugin.
     *
     * @return  array  An array of search areas.
     *
     * @since   1.6
     */
    public function onContentSearchAreas() {
        static $areas = array(
            'events' => 'PLG_SEARCH_EVENTS_EVENTS'
        );

        return $areas;
    }

    /**
     * Search content (events).
     *
     * The SQL must return the following fields that are used in a common display
     * routine: href, title, section, created, text, browsernav.
     *
     * @param   string  $text      Target search string.
     * @param   string  $phrase    Matching option (possible values: exact|any|all).  Default is "any".
     * @param   string  $ordering  Ordering option (possible values: newest|oldest|popular|alpha|category).  Default is "newest".
     * @param   mixed   $areas     An array if the search is to be restricted to areas or null to search all areas.
     *
     * @return  array  Search results.
     *
     * @since   1.6
     */
    public function onContentSearch($text, $phrase = '', $ordering = '', $areas = null) {

        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        $app = JFactory::getApplication();
        $groups = implode(',', $user->getAuthorisedViewLevels());
        $searchText = $text;

        $sContent = $this->params->get('search_event', 1);
        $sArchived = $this->params->get('search_archived', 1);
        $limit = $this->params->def('search_limit', 50);
        $state = array();


        if ($sContent) {
            $state[] = 1;
        }

        if ($sArchived) {
            $state[] = 2;
        }

        if (empty($state)) {
            return array();
        }

        $text = trim($text);

        if ($text === '') {
            return array();
        }
        $catid1 = $catid2 = $catid3 = $catid4 = '';

        if (is_array($areas)) {
            if (count($areas)) {
                for ($i = 0; $i < count($areas); $i++) {
                    //echo $areas[$i] . '<br>';
                    if (strpos($areas[$i], '1opt')) {
                        $catid1 = str_replace('ar1opt', '', $areas[$i]);
                    } else if ($areas[$i] == 'events') {
                        $catid2 = $areas[$i];
                    } else if (strpos($areas[$i], '3opt')) {
                        $catid3 = str_replace('ar3opt', '', $areas[$i]);
                    } else if (strpos($areas[$i], '4opt')) {
                        $catid4 = str_replace('ar4opt', '', $areas[$i]);
                    }
                }
            }
        }
        /*
         * Search article
         */
        $strcatid = '';
        $results = array();
        $iscontent = false;
//        echo $catid1 . '<br>';
//        echo $catid2 . 'test';
//        echo $catid3 . '<br>';
//        echo $catid4 . '<br>';
        if ($catid1 != '') {
            $iscontent = true;
        } else if ($catid3 != '') {
            $iscontent = true;
        } elseif ($catid4 != '') {
            $iscontent = true;
        } else if ($catid2 == '') {
            $iscontent = true;
        }
        if ($iscontent) {
            $strcatid = $catid1 . ',' . $catid3 . ',' . $catid4;
            $strcatid = str_replace('i', ',', $strcatid);
            $arrCatID = explode(',', $strcatid);
            $arrCatID = array_filter($arrCatID);
            $catId = implode(',', $arrCatID);
            JLoader::register('ContentHelperRoute', JPATH_SITE . '/components/com_content/helpers/route.php');
            JLoader::register('SearchHelper', JPATH_ADMINISTRATOR . '/components/com_search/helpers/search.php');

            $serverType = $db->serverType;
            $nullDate = $db->getNullDate();
            $date = JFactory::getDate();
            $now = $date->toSql();
            switch ($phrase) {
                case 'exact':
                    $text = $db->quote('%' . $db->escape($text, true) . '%', false);
                    $wheres2 = array();
                    $wheres2[] = 'a.title LIKE ' . $text;
                    $wheres2[] = 'a.introtext LIKE ' . $text;
                    $wheres2[] = 'a.fulltext LIKE ' . $text;
                    $wheres2[] = 'a.metakey LIKE ' . $text;
                    $wheres2[] = 'a.metadesc LIKE ' . $text;

                    // Join over Fields.
                    $subQuery = $db->getQuery(true);
                    $subQuery->select("cfv.item_id")
                            ->from("#__fields_values AS cfv")
                            ->join('LEFT', '#__fields AS f ON f.id = cfv.field_id')
                            ->where('(f.context IS NULL OR f.context = ' . $db->q('com_content.article') . ')')
                            ->where('(f.state IS NULL OR f.state = 1)')
                            ->where('(f.access IS NULL OR f.access IN (' . $groups . '))')
                            ->where('cfv.value LIKE ' . $text);

                    // Filter by language.
                    if ($app->isClient('site') && JLanguageMultilang::isEnabled()) {
                        $subQuery->where('(f.language IS NULL OR f.language in (' . $db->quote($tag) . ',' . $db->quote('*') . '))');
                    }

                    if ($serverType == "mysql") {
                        /* This generates a dependent sub-query so do no use in MySQL prior to version 6.0 !
                         * $wheres2[] = 'a.id IN( '. (string) $subQuery.')';
                         */

                        $db->setQuery($subQuery);
                        $fieldids = $db->loadColumn();

                        if (count($fieldids)) {
                            $wheres2[] = 'a.id IN(' . implode(",", $fieldids) . ')';
                        }
                    } else {
                        $wheres2[] = $subQuery->castAsChar('a.id') . ' IN( ' . (string) $subQuery . ')';
                    }

                    $where = '(' . implode(') OR (', $wheres2) . ')';
                    break;

                case 'all':
                case 'any':
                default:
                    $words = explode(' ', $text);
                    $wheres = array();
                    $cfwhere = array();

                    foreach ($words as $word) {
                        $word = $db->quote('%' . $db->escape($word, true) . '%', false);
                        $wheres2 = array();
                        $wheres2[] = 'LOWER(a.title) LIKE LOWER(' . $word . ')';
                        $wheres2[] = 'LOWER(a.introtext) LIKE LOWER(' . $word . ')';
                        $wheres2[] = 'LOWER(a.fulltext) LIKE LOWER(' . $word . ')';
                        $wheres2[] = 'LOWER(a.metakey) LIKE LOWER(' . $word . ')';
                        $wheres2[] = 'LOWER(a.metadesc) LIKE LOWER(' . $word . ')';

                        if ($phrase === 'all') {
                            // Join over Fields.
                            $subQuery = $db->getQuery(true);
                            $subQuery->select("cfv.item_id")
                                    ->from("#__fields_values AS cfv")
                                    ->join('LEFT', '#__fields AS f ON f.id = cfv.field_id')
                                    ->where('(f.context IS NULL OR f.context = ' . $db->q('com_content.article') . ')')
                                    ->where('(f.state IS NULL OR f.state = 1)')
                                    ->where('(f.access IS NULL OR f.access IN (' . $groups . '))')
                                    ->where('LOWER(cfv.value) LIKE LOWER(' . $word . ')');

                            // Filter by language.
                            if ($app->isClient('site') && JLanguageMultilang::isEnabled()) {
                                $subQuery->where('(f.language IS NULL OR f.language in (' . $db->quote($tag) . ',' . $db->quote('*') . '))');
                            }

                            if ($serverType == "mysql") {
                                $db->setQuery($subQuery);
                                $fieldids = $db->loadColumn();

                                if (count($fieldids)) {
                                    $wheres2[] = 'a.id IN(' . implode(",", $fieldids) . ')';
                                }
                            } else {
                                $wheres2[] = $subQuery->castAsChar('a.id') . ' IN( ' . (string) $subQuery . ')';
                            }
                        } else {
                            $cfwhere[] = 'LOWER(cfv.value) LIKE LOWER(' . $word . ')';
                        }

                        $wheres[] = implode(' OR ', $wheres2);
                    }

                    if ($phrase === 'any') {
                        // Join over Fields.
                        $subQuery = $db->getQuery(true);
                        $subQuery->select("cfv.item_id")
                                ->from("#__fields_values AS cfv")
                                ->join('LEFT', '#__fields AS f ON f.id = cfv.field_id')
                                ->where('(f.context IS NULL OR f.context = ' . $db->q('com_content.article') . ')')
                                ->where('(f.state IS NULL OR f.state = 1)')
                                ->where('(f.access IS NULL OR f.access IN (' . $groups . '))')
                                ->where('(' . implode(($phrase === 'all' ? ') AND (' : ') OR ('), $cfwhere) . ')');

                        // Filter by language.
                        if ($app->isClient('site') && JLanguageMultilang::isEnabled()) {
                            $subQuery->where('(f.language IS NULL OR f.language in (' . $db->quote($tag) . ',' . $db->quote('*') . '))');
                        }

                        if ($serverType == "mysql") {
                            $db->setQuery($subQuery);
                            $fieldids = $db->loadColumn();

                            if (count($fieldids)) {
                                $wheres[] = 'a.id IN(' . implode(",", $fieldids) . ')';
                            }
                        } else {
                            $wheres[] = $subQuery->castAsChar('a.id') . ' IN( ' . (string) $subQuery . ')';
                        }
                    }

                    $where = '(' . implode(($phrase === 'all' ? ') AND (' : ') OR ('), $wheres) . ')';
                    break;
            }

            switch ($ordering) {
                case 'oldest':
                    $order = 'a.created ASC';
                    break;

                case 'popular':
                    $order = 'a.hits DESC';
                    break;

                case 'alpha':
                    $order = 'a.title ASC';
                    break;

                case 'category':
                    $order = 'c.title ASC, a.title ASC';
                    break;

                case 'newest':
                default:
                    $order = 'a.created DESC';
                    break;
            }

            $rows = array();
            $query = $db->getQuery(true);

            if (trim($catId) != '') {
                $where = 'a.catid IN(' . $catId . ')';
            }

            // Search articles.
            if ($sContent && $limit > 0) {
                $query->clear();

                // SQLSRV changes.
                $case_when = ' CASE WHEN ';
                $case_when .= $query->charLength('a.alias', '!=', '0');
                $case_when .= ' THEN ';
                $a_id = $query->castAsChar('a.id');
                $case_when .= $query->concatenate(array($a_id, 'a.alias'), ':');
                $case_when .= ' ELSE ';
                $case_when .= $a_id . ' END as slug';

                $case_when1 = ' CASE WHEN ';
                $case_when1 .= $query->charLength('c.alias', '!=', '0');
                $case_when1 .= ' THEN ';
                $c_id = $query->castAsChar('c.id');
                $case_when1 .= $query->concatenate(array($c_id, 'c.alias'), ':');
                $case_when1 .= ' ELSE ';
                $case_when1 .= $c_id . ' END as catslug';

                $query->select('a.title AS title, a.metadesc, a.metakey, a.created AS created, a.language, a.catid')
                        ->select($query->concatenate(array('a.introtext', 'a.fulltext')) . ' AS text')
                        ->select('c.title AS section, ' . $case_when . ',' . $case_when1 . ', ' . '\'2\' AS browsernav')
                        ->from('#__content AS a')
                        ->join('INNER', '#__categories AS c ON c.id=a.catid')
                        ->where(
                                '(' . $where . ') AND a.state=1 AND c.published = 1 AND a.access IN (' . $groups . ') '
                                . 'AND c.access IN (' . $groups . ')'
                                . 'AND (a.publish_up = ' . $db->quote($nullDate) . ' OR a.publish_up <= ' . $db->quote($now) . ') '
                                . 'AND (a.publish_down = ' . $db->quote($nullDate) . ' OR a.publish_down >= ' . $db->quote($now) . ')'
                        )
                        ->group('a.id, a.title, a.metadesc, a.metakey, a.created, a.language, a.catid, a.introtext, a.fulltext, c.title, a.alias, c.alias, c.id')
                        ->order($order);

                // Filter by language.
                if ($app->isClient('site') && JLanguageMultilang::isEnabled()) {
                    $query->where('a.language in (' . $db->quote($tag) . ',' . $db->quote('*') . ')')
                            ->where('c.language in (' . $db->quote($tag) . ',' . $db->quote('*') . ')');
                }

                $db->setQuery($query, 0, $limit);

                try {
                    $list = $db->loadObjectList();
                } catch (RuntimeException $e) {
                    $list = array();
                    JFactory::getApplication()->enqueueMessage(JText::_('JERROR_AN_ERROR_HAS_OCCURRED'), 'error');
                }

                if (isset($list)) {
                    foreach ($list as $key => $item) {
                        $list[$key]->href = ContentHelperRoute::getArticleRoute($item->slug, $item->catid, $item->language);
                    }
                }

                $rows[] = $list;
            }

            // Search archived content.
            if ($sArchived && $limit > 0) {
                $query->clear();

                // SQLSRV changes.
                $case_when = ' CASE WHEN ';
                $case_when .= $query->charLength('a.alias', '!=', '0');
                $case_when .= ' THEN ';
                $a_id = $query->castAsChar('a.id');
                $case_when .= $query->concatenate(array($a_id, 'a.alias'), ':');
                $case_when .= ' ELSE ';
                $case_when .= $a_id . ' END as slug';

                $case_when1 = ' CASE WHEN ';
                $case_when1 .= $query->charLength('c.alias', '!=', '0');
                $case_when1 .= ' THEN ';
                $c_id = $query->castAsChar('c.id');
                $case_when1 .= $query->concatenate(array($c_id, 'c.alias'), ':');
                $case_when1 .= ' ELSE ';
                $case_when1 .= $c_id . ' END as catslug';

                $query->select(
                        'a.title AS title, a.metadesc, a.metakey, a.created AS created, '
                        . $query->concatenate(array('a.introtext', 'a.fulltext')) . ' AS text,'
                        . $case_when . ',' . $case_when1 . ', '
                        . 'c.title AS section, \'2\' AS browsernav'
                );

                // .'CONCAT_WS("/", c.title) AS section, \'2\' AS browsernav' );
                $query->from('#__content AS a')
                        ->join('INNER', '#__categories AS c ON c.id=a.catid AND c.access IN (' . $groups . ')')
                        ->where(
                                '(' . $where . ') AND a.state = 2 AND c.published = 1 AND a.access IN (' . $groups
                                . ') AND c.access IN (' . $groups . ') '
                                . 'AND (a.publish_up = ' . $db->quote($nullDate) . ' OR a.publish_up <= ' . $db->quote($now) . ') '
                                . 'AND (a.publish_down = ' . $db->quote($nullDate) . ' OR a.publish_down >= ' . $db->quote($now) . ')'
                        )
                        ->order($order);

                // Join over Fields is no longer neded
                // Filter by language.
                if ($app->isClient('site') && JLanguageMultilang::isEnabled()) {
                    $query->where('a.language in (' . $db->quote($tag) . ',' . $db->quote('*') . ')')
                            ->where('c.language in (' . $db->quote($tag) . ',' . $db->quote('*') . ')');
                }
                //echo $query;
                $db->setQuery($query, 0, $limit);

                try {
                    $list3 = $db->loadObjectList();
                } catch (RuntimeException $e) {
                    $list3 = array();
                    JFactory::getApplication()->enqueueMessage(JText::_('JERROR_AN_ERROR_HAS_OCCURRED'), 'error');
                }

                // Find an itemid for archived to use if there isn't another one.
                $item = $app->getMenu()->getItems('link', 'index.php?option=com_content&view=archive', true);
                $itemid = isset($item->id) ? '&Itemid=' . $item->id : '';

                if (isset($list3)) {
                    foreach ($list3 as $key => $item) {
                        $date = JFactory::getDate($item->created);

                        $created_month = $date->format('n');
                        $created_year = $date->format('Y');

                        $list3[$key]->href = JRoute::_('index.php?option=com_content&view=archive&year=' . $created_year . '&month=' . $created_month . $itemid);
                    }
                }

                $rows[] = $list3;
            }

            if (count($rows)) {
                foreach ($rows as $row) {
                    $new_row = array();

                    foreach ($row as $article) {
                        // Not efficient to get these ONE article at a TIME
                        // Lookup field values so they can be checked, GROUP_CONCAT would work in above queries, but isn't supported by non-MySQL DBs.
                        $query = $db->getQuery(true);
                        $query->select('fv.value')
                                ->from('#__fields_values as fv')
                                ->join('left', '#__fields as f on fv.field_id = f.id')
                                ->where('f.context = ' . $db->quote('com_content.article'))
                                ->where('fv.item_id = ' . $db->quote((int) $article->slug));
                        $db->setQuery($query);
                        $article->jcfields = implode(',', $db->loadColumn());

                        if (SearchHelper::checkNoHtml($article, $searchText, array('text', 'title', 'jcfields', 'metadesc', 'metakey'))) {
                            $new_row[] = $article;
                            $limit--;
                        } elseif (trim($catId) != '') {
                            //$new_row[] = $article;
                        }
                    }

                    $results = array_merge($results, (array) $new_row);
                }
            }
        }
        /*
         * Search event
         */
        $return = array();
        $isevent = false;
        if ($catid1 == '' && $catid3 == '' && $catid4 == '') {
            $isevent = true;
        } else if ($catid2 != '') {
            $isevent = true;
        }
        if ($isevent) {
            switch ($ordering) {
                case 'alpha':
                    $order = 'a.title ASC';
                    break;

                case 'category':
                case 'popular':
                case 'newest':
                case 'oldest':
                default:
                    $order = 'a.title DESC';
            }

            $text = $db->quote('%' . $db->escape($text, true) . '%', false);
            $query = $db->getQuery(true);

            // SQLSRV changes.
            $case_when = ' CASE WHEN ';
            $case_when .= $query->charLength('a.alias', '!=', '0');
            $case_when .= ' THEN ';
            $a_id = $query->castAsChar('a.id');
            $case_when .= $query->concatenate(array($a_id, 'a.alias'), ':');
            $case_when .= ' ELSE ';
            $case_when .= $a_id . ' END as slug';
            $query->select('a.id, a.title, a.description AS text, a.time_created AS created, \'2\' AS browsernav, ' . $case_when)
                    ->from('#__events_event AS a')
                    ->where(
                            '(a.title LIKE ' . $text . ' OR a.description LIKE ' . $text . ')'
                    )
                    ->group('a.id, a.title, a.description, a.time_created, a.alias')
                    ->order($order);

            $db->setQuery($query, 0, $limit);

            try {
                $rows = $db->loadObjectList();
            } catch (RuntimeException $e) {
                $rows = array();
                JFactory::getApplication()->enqueueMessage(JText::_('JERROR_AN_ERROR_HAS_OCCURRED'), 'error');
            }

            if ($rows) {
                foreach ($rows as $i => $row) {
                    if (searchHelper::checkNoHtml($row, $searchText, array('name', 'title', 'text'))) {
                        $row->href = JRoute::_('index.php?option=com_events&view=event&id=' . (int) $row->id);
                        $row->section = JText::_('JCATEGORY');

                        $return[] = $row;
                    }
                }
            }
        }
        if (count($results)) {
            $return = array_merge($results, (array) $return);
        }
        return $return;
    }

}
