<?php

/*
 * Created on : Feb 12, 2018, 10:17:38 AM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */

defined('_JEXEC') or die;

class PlgSystemLogvisitor extends JPlugin {

    public function onAfterRender() {
        // Do something onAfterRoute

        //$address = $this->getLocationInfoByIp();
        file_put_contents(dirname(__FILE__) . '/logvisitor.txt', date('d/m/Y') . ' - ' . $_SERVER['REMOTE_ADDR'] . ' - ' . $_SERVER['REQUEST_URI'] . PHP_EOL, FILE_APPEND);
    }

    function getLocationInfoByIp() {
        $ipaddress = '';
        if (@$_SERVER['REMOTE_ADDR'] != '127.0.0.1')
            $ipaddress = @$_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ipaddress));
        if ($ip_data && $ip_data->geoplugin_countryName != null) {
            $result['country'] = $ip_data->geoplugin_countryCode;
            $result['city'] = $ip_data->geoplugin_city;
            $result['ipaddress'] = $ipaddress;
        } else {
            $result['country'] = 'UNKNOWN';
            $result['city'] = 'UNKNOWN';
            $result['ipaddress'] = $ipaddress;
        }
        return $result;
    }

}
