<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;

jimport('joomla.utilities.date');
jimport('joomla.form.form');

/**
 * author: Tran Trong Thang
 */
class PlgSystemTVplgarticle extends JPlugin {

    /**
     * Date of birth.
     *
     * @var    string
     * @since  3.1
     */
    private $date = '';

    /**
     * Load the language file on instantiation.
     *
     * @var    boolean
     * @since  3.1
     */
    protected $autoloadLanguage = true;

    /**
     * Constructor
     *
     * @param   object  &$subject  The object to observe
     * @param   array   $config    An array that holds the plugin configuration
     *
     * @since   1.5
     */
    public function __construct(& $subject, $config) {
        parent::__construct($subject, $config);
    }

    /**
     * adds additional fields to the user editing form
     *
     * @param   JForm  $form  The form to be altered.
     * @param   mixed  $data  The associated data for the form.
     *
     * @return  boolean
     *
     * @since   1.6
     */
    public function onContentPrepareForm($form, $data) {
        if (!($form instanceof JForm)) {
            $this->_subject->setError('JERROR_NOT_A_FORM');
            return false;
        }
        // Check we are manipulating a valid form.
        $name = $form->getName();
        if (!in_array($name, array('com_content.article'))) {
            return true;
        }
        // Add the registration fields to the form.
        JForm::addFormPath(__DIR__ . '/tvplgarticle');
        $form->loadFile('tvplgarticle', false);
        ?>
        <?php
        return true;
    }

}
