<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Events
 * @author     Thang Tran <trantrongthang1207@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Events model.
 *
 * @since  1.6
 */
class EventsModelEvent extends JModelAdmin {

    /**
     * @var      string    The prefix to use with controller messages.
     * @since    1.6
     */
    protected $text_prefix = 'COM_EVENTS';

    /**
     * @var   	string  	Alias to manage history control
     * @since   3.2
     */
    public $typeAlias = 'com_events.event';

    /**
     * @var null  Item data
     * @since  1.6
     */
    protected $item = null;

    /**
     * Returns a reference to the a Table object, always creating it.
     *
     * @param   string  $type    The table type to instantiate
     * @param   string  $prefix  A prefix for the table class name. Optional.
     * @param   array   $config  Configuration array for model. Optional.
     *
     * @return    JTable    A database object
     *
     * @since    1.6
     */
    public function getTable($type = 'Event', $prefix = 'EventsTable', $config = array()) {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Method to get the record form.
     *
     * @param   array    $data      An optional array of data for the form to interogate.
     * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
     *
     * @return  JForm  A JForm object on success, false on failure
     *
     * @since    1.6
     */
    public function getForm($data = array(), $loadData = true) {
        // Initialise variables.
        $app = JFactory::getApplication();

        // Get the form.
        $form = $this->loadForm(
                'com_events.event', 'event', array('control' => 'jform',
            'load_data' => $loadData
                )
        );

        if (empty($form)) {
            return false;
        }

        return $form;
    }

    /**
     * Method to get the data that should be injected in the form.
     *
     * @return   mixed  The data for the form.
     *
     * @since    1.6
     */
    protected function loadFormData() {
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState('com_events.edit.event.data', array());

        if (empty($data)) {
            if ($this->item === null) {
                $this->item = $this->getItem();
            }

            $data = $this->item;
        }

        return $data;
    }

    /**
     * Method to get a single record.
     *
     * @param   integer  $pk  The id of the primary key.
     *
     * @return  mixed    Object on success, false on failure.
     *
     * @since    1.6
     */
    public function getItem($pk = null) {
        if ($item = parent::getItem($pk)) {
            // Do any procesing on fields here if needed
        }

        return $item;
    }

    /**
     * Method to duplicate an Event
     *
     * @param   array  &$pks  An array of primary key IDs.
     *
     * @return  boolean  True if successful.
     *
     * @throws  Exception
     */
    public function duplicate(&$pks) {
        $user = JFactory::getUser();

        // Access checks.
        if (!$user->authorise('core.create', 'com_events')) {
            throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
        }

        $dispatcher = JEventDispatcher::getInstance();
        $context = $this->option . '.' . $this->name;

        // Include the plugins for the save events.
        JPluginHelper::importPlugin($this->events_map['save']);

        $table = $this->getTable();

        foreach ($pks as $pk) {
            if ($table->load($pk, true)) {
                // Reset the id to create a new record.
                $table->id = 0;

                if (!$table->check()) {
                    throw new Exception($table->getError());
                }

                if (!empty($table->images)) {
                    if (is_array($table->images)) {
                        $table->images = implode(',', $table->images);
                    }
                } else {
                    $table->images = '';
                }


                // Trigger the before save event.
                $result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

                if (in_array(false, $result, true) || !$table->store()) {
                    throw new Exception($table->getError());
                }

                // Trigger the after save event.
                $dispatcher->trigger($this->event_after_save, array($context, &$table, true));
            } else {
                throw new Exception($table->getError());
            }
        }

        // Clean cache
        $this->cleanCache();

        return true;
    }

    /**
     * Prepare and sanitise the table prior to saving.
     *
     * @param   JTable  $table  Table Object
     *
     * @return void
     *
     * @since    1.6
     */
    protected function prepareTable($table) {
        jimport('joomla.filter.output');

        if (empty($table->id)) {
            // Set ordering to the last item if not set
            if (@$table->ordering === '') {
                $db = JFactory::getDbo();
                $db->setQuery('SELECT MAX(ordering) FROM #__events_event');
                $max = $db->loadResult();
                $table->ordering = $max + 1;
            }
        }
    }

    public function save($data) {

        $select_from_start = JRequest::getVar('select_from_start');
        $select_to_start = JRequest::getVar('select_to_start');

        $selhour_from_start = JRequest::getVar('selhour_from_start');
        $selhour_to_start = JRequest::getVar('selhour_to_start');

        $selminute_from_start = JRequest::getInt('selminute_from_start', 0);
        $selminute_to_start = JRequest::getInt('selminute_to_start', 0);

        if ($selhour_from_start) {
            if ($selhour_to_start)
                $time_start = $selhour_from_start . ':' . $selminute_from_start . ' ' . $select_from_start . ' - ' . $selhour_to_start . ':' . $selminute_to_start . ' ' . $select_to_start;
            else
                $time_start = $selhour_from_start . ':' . $selminute_from_start . ' ' . $select_from_start;
        } else {
            $time_start = "";
        }
        $data['time_start'] = $time_start;

        $select_from_end = JRequest::getVar('select_from_end');
        $select_to_end = JRequest::getVar('select_to_end');

        $selhour_from_end = JRequest::getVar('selhour_from_end');
        $selhour_to_end = JRequest::getVar('selhour_to_end');

        $selminute_from_end = JRequest::getInt('selminute_from_end', 0);
        $selminute_to_end = JRequest::getInt('selminute_to_end', 0);

        if ($selhour_from_end) {
            if ($selhour_to_end)
                $time_end = $selhour_from_end . ':' . $selminute_from_end . ' ' . $select_from_end . ' - ' . $selhour_to_end . ':' . $selminute_to_end . ' ' . $select_to_end;
            else
                $time_end = $selhour_from_end . ':' . $selminute_from_end . ' ' . $select_from_end;
        } else {
            $time_end = "";
        }
        $data['time_end'] = $time_end;
        if ($time_end != '') {
            $data['time_start'] = $time_end;
        }

        if (parent::save($data)) {
            return true;
        }
        return false;
    }

}
