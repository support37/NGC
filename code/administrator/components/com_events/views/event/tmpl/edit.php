<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Events
 * @author     Thang Tran <trantrongthang1207@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_events/css/form.css');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function () {

    });

    Joomla.submitbutton = function (task) {
        if (task == 'event.cancel') {
            Joomla.submitform(task, document.getElementById('event-form'));
        } else {

            if (task != 'event.cancel' && document.formvalidator.isValid(document.id('event-form'))) {

                Joomla.submitform(task, document.getElementById('event-form'));
            } else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form
    action="<?php echo JRoute::_('index.php?option=com_events&layout=edit&id=' . (int) $this->item->id); ?>"
    method="post" enctype="multipart/form-data" name="adminForm" id="event-form" class="form-validate">

    <div class="form-horizontal">
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EVENTS_TITLE_EVENT', true)); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform">

                    <input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
                    <input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
                    <input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
                    <input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
                    <input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

                    <?php echo $this->form->renderField('created_by'); ?>
                    <?php echo $this->form->renderField('modified_by'); ?>		
                    <?php echo $this->form->renderField('title'); ?>
                    <?php echo $this->form->renderField('alias'); ?>
                    <?php echo $this->form->renderField('date_start'); ?>
                    <?php //echo $this->form->renderField('time'); ?>
                    <?php
                    if (!$this->item->time_start) {
                        $from[0] = "";
                        $from[1] = "";
                        $to[0] = "";
                        $to[1] = "";
                    } else {
                        $from_to = explode("-", $this->item->time_start);
                        $from = explode(":", $from_to[0]);
                        if (isset($from_to[1]))
                            $to = explode(":", $from_to[1]);
                        else {
                            $to[0] = "";
                            $to[1] = "";
                        }
                    }
                    ?> 
                    <div class="control-group hide">
                        <div class="control-label">
                            <label for="jform_address" id="jform_address-lbl">
                                <?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_TIME_START', true); ?>
                            </label>
                        </div>
                        <div class="controls">

                            <input type="text" id="selhour_from_start" name="selhour_from_start" size="1" style="text-align:right;width:30px" onkeypress="return check12hour('selhour_from_start')" value="<?php echo $from[0]; ?>" title="from"  onblur="add_0('selhour_from_start')"/> <b>:</b>
                            <input type="text" id="selminute_from_start" name="selminute_from_start" size="1" style="text-align:right;width:30px" onkeypress="return checkminute('selminute_from_start')" value="<?php echo substr($from[1], 0, 2); ?>"  title="from" onblur="add_0('selminute_from_start')"/> 
                            <select id="select_from" style="width:60px" name="select_from_start" >
                                <option value="AM" <?php if (substr($from[1], 3, 2) == "AM") echo 'selected="selected"'; ?>>AM</option>
                                <option value="PM" <?php if (substr($from[1], 3, 2) == "PM") echo 'selected="selected"'; ?>>PM</option>

                            </select>

                            <span style="font-size:12px">&nbsp;-&nbsp;</span>

                            <input type="text" id="selhour_to_start" name="selhour_to_start" size="1" style="text-align:right;width:30px" onkeypress="return check12hour('selhour_to_start')" value="<?php echo $to[0]; ?>"  title="to" onblur="add_0('selhour_to_start')" /> <b>:</b>
                            <input type="text" id="selminute_to_start" name="selminute_to_start" size="1" style="text-align:right;width:30px" onkeypress="return checkminute('selminute_to_start')" value="<?php echo substr($to[1], 0, 2); ?>"  title="to" onblur="add_0('selminute_to_start')"/>

                            <select id="select_to" style="width:60px" name="select_to_start">
                                <option value="AM" <?php if (substr($to[1], 3, 2) == "AM") echo 'selected="selected"'; ?>>AM</option>
                                <option value="PM" <?php if (substr($to[1], 3, 2) == "PM") echo 'selected="selected"'; ?>>PM</option>

                            </select>

                        </div>
                    </div>
                    <?php echo $this->form->renderField('date_end'); ?>
                    <?php
                    if (!$this->item->time_end) {
                        $from[0] = "";
                        $from[1] = "";
                        $to[0] = "";
                        $to[1] = "";
                    } else {
                        $from_to = explode("-", $this->item->time_end);
                        $from = explode(":", $from_to[0]);
                        if (isset($from_to[1]))
                            $to = explode(":", $from_to[1]);
                        else {
                            $to[0] = "";
                            $to[1] = "";
                        }
                    }
                    ?> 
                    <div class="control-group">
                        <div class="control-label">
                            <label for="jform_address" id="jform_address-lbl">
                                <?php echo JText::_('COM_EVENTS_FORM_LBL_EVENT_TIME', true); ?>
                            </label>
                        </div>
                        <div class="controls">

                            <input type="text" id="selhour_from_end" name="selhour_from_end" size="1" style="text-align:right;width:30px" onkeypress="return check12hour('selhour_from_end')" value="<?php echo $from[0]; ?>" title="from"  onblur="add_0('selhour_from_end')"/> <b>:</b>
                            <input type="text" id="selminute_from_end" name="selminute_from_end" size="1" style="text-align:right;width:30px" onkeypress="return checkminute('selminute_from_end')" value="<?php echo substr($from[1], 0, 2); ?>"  title="from" onblur="add_0('selminute_from_end')"/> 
                            <select id="select_from" style="width:60px" name="select_from_end" >
                                <option value="AM" <?php if (substr($from[1], 3, 2) == "AM") echo 'selected="selected"'; ?>>AM</option>
                                <option value="PM" <?php if (substr($from[1], 3, 2) == "PM") echo 'selected="selected"'; ?>>PM</option>

                            </select>

                            <span style="font-size:12px">&nbsp;-&nbsp;</span>

                            <input type="text" id="selhour_to_end" name="selhour_to_end" size="1" style="text-align:right;width:30px" onkeypress="return check12hour('selhour_to_end')" value="<?php echo $to[0]; ?>"  title="to" onblur="add_0('selhour_to_end')" /> <b>:</b>
                            <input type="text" id="selminute_to_end" name="selminute_to_end" size="1" style="text-align:right;width:30px" onkeypress="return checkminute('selminute_to_end')" value="<?php echo substr($to[1], 0, 2); ?>"  title="to" onblur="add_0('selminute_to_end')"/>

                            <select id="select_to" style="width:60px" name="select_to_end">
                                <option value="AM" <?php if (substr($to[1], 3, 2) == "AM") echo 'selected="selected"'; ?>>AM</option>
                                <option value="PM" <?php if (substr($to[1], 3, 2) == "PM") echo 'selected="selected"'; ?>>PM</option>

                            </select>

                        </div>
                    </div>
                    <?php echo $this->form->renderField('address'); ?>
                    <?php echo $this->form->renderField('description'); ?>
                    <?php echo $this->form->renderField('images'); ?>

                    <?php if (!empty($this->item->images)) : ?>
                        <?php $imagesFiles = array(); ?>
                        <?php foreach ((array) $this->item->images as $fileSingle) : ?>
                            <?php if (!is_array($fileSingle)) : ?>
                                <a href="<?php echo JRoute::_(JUri::root() . 'uploads' . DIRECTORY_SEPARATOR . $fileSingle, false); ?>"><img src="<?php echo JRoute::_(JUri::root() . 'uploads/' . $fileSingle, false); ?>" width="200"/></a> | 
                                <?php $imagesFiles[] = $fileSingle; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <input type="hidden" name="jform[images_hidden]" id="jform_images_hidden" value="<?php echo implode(',', $imagesFiles); ?>" />
                    <?php endif; ?>	
                    <?php //echo $this->form->renderField('repeat_method'); ?>
                    <?php //echo $this->form->renderField('repeat'); ?>
                    <?php //echo $this->form->renderField('week'); ?>
                    <?php //echo $this->form->renderField('month'); ?>
                    <?php //echo $this->form->renderField('month_type'); ?>
                    <?php //echo $this->form->renderField('monthly_list'); ?>
                    <?php //echo $this->form->renderField('month_week'); ?>
                    <?php //echo $this->form->renderField('year_month'); ?>


                    <?php if ($this->state->params->get('save_history', 1)) : ?>
                        <div class="control-group">
                            <div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
                            <div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
                        </div>
                    <?php endif; ?>
                </fieldset>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>



        <?php echo JHtml::_('bootstrap.endTabSet'); ?>

        <input type="hidden" name="task" value=""/>
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>
<script>
    function check12hour(id)
    {
        if (typeof (event) != 'undefined')
        {
            var e = event; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;
            input = document.getElementById(id);


            if (charCode == 48 && input.value.length == 0)
                return false;

            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            hour = "" + document.getElementById(id).value + String.fromCharCode(e.charCode);
            hour = parseFloat(hour);
            if (document.getSelection() != '')
                return true;

            if ((hour < 0) || (hour > 12))
                return false;
        }
        return true;
    }
    function checkminute(id)
    {
        if (typeof (event) != 'undefined')
        {
            var e = event; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            minute = "" + document.getElementById(id).value + String.fromCharCode(e.charCode);
            minute = parseFloat(minute);
            if (document.getSelection() != '')
                return true;
            if ((minute < 0) || (minute > 59))
                return false;
        }
        return true;
    }
    function add_0(id)
    {

        input = document.getElementById(id);

        if (input.value.length == 1)

        {

            input.value = '0' + input.value;

            input.setAttribute("value", input.value);

        }

    }
</script>