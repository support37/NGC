<?php
namespace Twilio;

/**
 *
 * @package JCHAT::FRAMEWORK::administrator::components::com_jchat
 * @subpackage framework
 * @subpackage twilio
 * @subpackage sdk
 * @author Joomla! Extensions Store
 * @copyright (C) 2015 - Joomla! Extensions Store
 * @license GNU/GPLv2 http://www.gnu.org/licenses/gpl-2.0.html
 *
 */
defined ( '_JEXEC' ) or die ( 'Restricted access' );

class Deserialize {

    public static function iso8601DateTime($s) {
        try {
            return new \DateTime($s, new \DateTimeZone('UTC'));
        } catch (\Exception $e) {
            return $s;
        }
    }

}