
/*
 * Created on : Jan 23, 2018, 9:03:25 PM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */
jQuery(document).ready(function ($) {

    $('#vina-slideshow-content526').sliderPro({
        width: 485,
        height: 300,
        startSlide: 0,
        shuffle: true,
        orientation: 'horizontal',
        forceSize: 'none',
        loop: true,
        slideDistance: 0,
        slideAnimationDuration: 700,
        heightAnimationDuration: 700,
        fade: false,
        fadeOutPreviousSlide: true,
        fadeDuration: 500,
        autoplay: false,
        autoplayDelay: 5000,
        autoplayOnHover: 'pause',
        arrows: true,
        fadeArrows: true,
        touchSwipe: false,
        fadeCaption: true,
        captionFadeDuration: 500,
        thumbnailWidth: 250,
        thumbnailHeight: 97,
        thumbnailsPosition: 'right',
        thumbnailPointer: true,
        thumbnailArrows: true,
        buttons: false,
        autoScaleLayers: false,
        breakpoints: {
            800: {
                thumbnailsPosition: 'bottom',
                thumbnailWidth: 220,
                thumbnailHeight: 95
            },
            500: {
                thumbnailsPosition: 'bottom',
                thumbnailWidth: 120,
                thumbnailHeight: 95
            }}
    });

    $('ul.nav.tv-tab .parent').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });
});