<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');

$lang = JFactory::getLanguage();
$upper_limit = $lang->getUpperLimitSearchWord();
//print_r($this->searchareas['active']);
?>
<form id="searchForm" action="<?php echo JRoute::_('index.php?option=com_search'); ?>" method="post">
    <div class="btn-toolbar">
        <div class="btn-group pull-left">
            <input type="text" name="searchword" title="<?php echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?>" placeholder="<?php echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?>" id="search-searchword" size="30" maxlength="<?php echo $upper_limit; ?>" value="<?php echo $this->escape($this->origkeyword); ?>" class="inputbox" />
        </div>
        <div class="btn-group pull-left">
            <button name="Search" onclick="this.form.submit()" class="btn hasTooltip" title="<?php echo JHtml::_('tooltipText', 'COM_SEARCH_SEARCH'); ?>">
                <span class="icon-search"></span>
                <?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>
            </button>
        </div>
        <input type="hidden" name="task" value="search" />
        <div class="clearfix"></div>
    </div>
    <div class="searchintro<?php echo $this->params->get('pageclass_sfx'); ?>">
        <?php if (!empty($this->searchword)) : ?>
            <p>
                <?php echo JText::plural('COM_SEARCH_SEARCH_KEYWORD_N_RESULTS', '<span class="badge badge-info">' . $this->total . '</span>'); ?>
            </p>
        <?php endif; ?>
    </div>
    <?php if ($this->params->get('search_phrases', 1)) : ?>
        <fieldset class="phrases">
            <legend>
                <?php echo JText::_('COM_SEARCH_FOR'); ?>
            </legend>
            <div class="phrases-box">
                <?php echo $this->lists['searchphrase']; ?>
            </div>
            <div class="ordering-box">
                <label for="ordering" class="ordering">
                    <?php echo JText::_('COM_SEARCH_ORDERING'); ?>
                </label>
                <?php echo $this->lists['ordering']; ?>
            </div>
        </fieldset>
    <?php endif; ?>
    <?php
    if ($this->params->get('search_areas', 1)) :
        $plugin = JPluginHelper::getPlugin('search', 'events');
        if ($plugin) {
            // Get plugin params
            $pluginParams = new JRegistry($plugin->params);

            $option1 = $pluginParams->get('option1');
            $option2 = $pluginParams->get('option2');
            $option3 = $pluginParams->get('option3');
            $option4 = $pluginParams->get('option4');

            $catid1 = $pluginParams->get('catid1');
            $catid2 = $pluginParams->get('catid2');
            $catid3 = $pluginParams->get('catid3');
            $catid4 = $pluginParams->get('catid4');
        }
        ?>
        <fieldset class="only">
            <legend>
                <?php echo JText::_('COM_SEARCH_SEARCH_ONLY'); ?>
            </legend>
            <?php if (trim($catid1) != '') { ?>
                <label for="area-option1" class="checkbox">
                    <?php $val1 = is_array($catid1) ? implode('i', $catid1) : $catid1 ?>
                    <?php $checked = is_array($this->searchareas['active']) && in_array('ar1opt' . $val1, $this->searchareas['active']) ? 'checked="checked"' : ''; ?>
                    <input type="checkbox" name="areas[1]" value="ar1opt<?php echo $val1 ?>" id="area-option1" <?php echo $checked; ?>>
                    <?php echo JText::_($option1); ?>   
                </label>
            <?php } ?>
            <?php if ($catid2) { ?>
                <label for="area-option2" class="checkbox">
                    <?php
                    $checked = '';
                    $checked = is_array($this->searchareas['active']) && in_array('events', $this->searchareas['active']) ? 'checked="checked"' : '';
                    ?>
                    <input type="checkbox" name="areas[2]" value="events" id="area-option2" <?php echo $checked; ?>>
                    <?php echo JText::_($option2); ?>   
                </label>
            <?php } ?>
            <?php if (trim($catid3) != '') { ?>
                <label for="area-option3" class="checkbox">
                    <?php
                    $val3 = is_array($catid3) ? implode('i', $catid3) : $catid3;
                    $checked = '';
                    $checked = is_array($this->searchareas['active']) && in_array('ar3opt' . $val3, $this->searchareas['active']) ? 'checked="checked"' : '';
                    ?>
                    <input type="checkbox" name="areas[3]" value="ar3opt<?php echo $val3 ?>" id="area-option3" <?php echo $checked; ?>>
                    <?php echo JText::_($option3); ?>   
                </label>
            <?php } ?>
            <?php if (is_array($catid4)) { ?>
                <label for="area-option4" class="checkbox">
                    <?php
                    $val4 = is_array($catid4) ? implode('i', $catid4) : $catid4;
                    $checked = '';
                    $checked = is_array($this->searchareas['active']) && in_array('ar4opt' . $val4, $this->searchareas['active']) ? 'checked="checked"' : '';
                    ?>
                    <input type="checkbox" name="areas[4]" value="ar4opt<?php echo $val4 ?>" id="area-option4" <?php echo $checked; ?>>
                    <?php echo JText::_($option4); ?>   
                </label>
            <?php } ?>
        </fieldset>
    <?php endif; ?>
    <?php if ($this->total > 0) : ?>
        <div class="form-limit">
            <label for="limit">
                <?php echo JText::_('JGLOBAL_DISPLAY_NUM'); ?>
            </label>
            <?php echo $this->pagination->getLimitBox(); ?>
        </div>
        <p class="counter">
            <?php echo $this->pagination->getPagesCounter(); ?>
        </p>
    <?php endif; ?>
</form>
