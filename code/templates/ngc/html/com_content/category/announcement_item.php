<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Create a shortcut for params.
$params = $this->item->params;
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
$canEdit = $this->item->params->get('access-edit');
$info = $params->get('info_block_position', 0);

// Check if associations are implemented. If they are, define the parameter.
$assocParam = (JLanguageAssociations::isEnabled() && $params->get('show_associations'));
?>

<h3>
    <span class="fulltitle"><?php echo $this->escape($this->item->title); ?></span>
    <span class="lesstitle"><?php echo characterLimit($this->escape($this->item->title), $this->item->limitchar, '...'); ?></span>
    <span class="pull-right mess-date"><?php echo JHtml::_('date', $this->item->created, 'd/m/Y H:i:s a'); ?></span><span class="more-less"></span>
</h3>
<div class="sub-mess"><?php echo $this->item->introtext; ?></div>
<div class="content-mess">
    <?php echo $this->item->fulltext; ?>
</div>
