<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Create a shortcut for params.
$params = $this->item->params;
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
$canEdit = $this->item->params->get('access-edit');
$info = $params->get('info_block_position', 0);

// Check if associations are implemented. If they are, define the parameter.
$assocParam = (JLanguageAssociations::isEnabled() && $params->get('show_associations'));
$this->item->slug = $this->item->id . ':' . $this->item->alias;
$image = json_decode($this->item->images);
$this->item->link = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language));

$app = JFactory::getApplication();

// Getting params from template
$params = $app->getTemplate(true)->params;
$noimagearticle = $params->get('noimagearticle', '');
?>

<div class="box-department">
    <div class="row">
        <div class="col-md-3">
            <?php if ($image->image_intro != '') : ?>
                <a href="<?php echo $this->item->link; ?>">
                    <img src="<?php echo  JUri::root() . $image->image_intro . ''; ?>"/>
                </a> 
                <?php
            else:
                if (file_exists(JPATH_ROOT . '/' . $noimagearticle)):
                    ?>
                    <a href="<?php echo $this->item->link; ?>">
                        <img src="<?php echo  JUri::root() . $noimagearticle . ''; ?>"/>
                    </a> 
                    <?php
                else:
                    ?>
                    <a href="<?php echo $this->item->link; ?>">
                        <img src="<?php echo  JUri::root() . 'components/com_events/assets/images/noimageevent.png'; ?>"/>
                    </a> 
                <?php
                endif;
            endif;
            ?>
        </div>
        <div class="col-md-9">
            <h4 class="bold"><a href="<?php echo $this->item->link; ?>"><?php echo $this->escape($this->item->title); ?> </a></h4>
            <div class="short-description"><?php echo $this->item->introtext; ?></div>
            <p class="blog-date">
                <span class="calander_fd">&nbsp;</span><?php echo JHtml::_('date', $this->item->created, JText::_('d F Y')); ?>
            </p>
        </div>
    </div>
</div>
<hr/>