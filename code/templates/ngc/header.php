<?php
/*
 * Created on : Jan 22, 2018, 9:12:05 PM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */
?>
<link href="templates/<?php echo $this->template ?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="templates/<?php echo $this->template ?>/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="templates/<?php echo $this->template ?>/assets/css/slider-pro.css" rel="stylesheet" type="text/css"/>
<link href="templates/<?php echo $this->template ?>/assets/css/owl.carousel.min.css" rel="stylesheet" type="text/css"/>
<link href="templates/<?php echo $this->template ?>/assets/css/owl.theme.default.min.css" rel="stylesheet" type="text/css"/>
<link href="templates/<?php echo $this->template ?>/assets/css/tvstyle.css" rel="stylesheet" type="text/css"/>
<link href="templates/<?php echo $this->template ?>/assets/css/custom.css" rel="stylesheet" type="text/css"/>

<!--<script src="templates/<?php echo $this->template ?>/assets/js/jquery.min.js" type="text/javascript"></script>-->
<script src="templates/<?php echo $this->template ?>/assets/js/vendor/popper.min.js" type="text/javascript"></script>
<script src="templates/<?php echo $this->template ?>/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="templates/<?php echo $this->template ?>/assets/js/jquery.sliderPro.js" type="text/javascript"></script>
<script src="templates/<?php echo $this->template ?>/assets/js/owl.carousel.min.js" type="text/javascript"></script>
<script src="templates/<?php echo $this->template ?>/assets/js/tvscript.js" type="text/javascript"></script>
<script src="templates/<?php echo $this->template ?>/assets/js/custom.js" type="text/javascript"></script>