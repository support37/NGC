<?php 
function mydap_start($username,$password,$host,$port=389) {
	global $mydap;
	if(isset($mydap)) die('Error, LDAP connection already established');
	$mydap = ldap_connect($host,$port) or die('Error connecting to LDAP');
	ldap_set_option($mydap, LDAP_OPT_PROTOCOL_VERSION,3);
        ldap_set_option($mydap, LDAP_OPT_REFERRALS,0);
	@ldap_bind($mydap,$username,$password) or die('Error binding to LDAP: '.ldap_error($mydap));
	return true;
}

function mydap_end() {
	global $mydap;
	if(!isset($mydap)) die('Error, no LDAP connection established');
	ldap_unbind($mydap);
}

function mydap_attributes($user_dn,$keep=false) {
	global $mydap;
	if(!isset($mydap)) die('Error, no LDAP connection established');
	if(empty($user_dn)) die('Error, no LDAP user specified');
	ldap_control_paged_result($mydap,1);
	$results = (($keep) ? ldap_search($mydap,$user_dn,'cn=*',$keep) : ldap_search($mydap,$user_dn,'cn=*'))
	or die('Error searching LDAP: '.ldap_error($mydap));
	$attributes = ldap_get_entries($mydap,$results);
//var_dump($attributes);
	if(isset($attributes[0])) return $attributes[0];
	else return array();
}

function mydap_members($object_dn,$object_class='g') {
	global $mydap;
	if(!isset($mydap)) die('Error, no LDAP connection established');
	if(empty($object_dn)) die('Error, no LDAP object specified');
	$pagesize = 1000;
$output=array();
		$counter = "";
	do {
		ldap_control_paged_result($mydap,$pagesize,true,$counter);
		$results = ldap_search($mydap,$object_dn,'objectClass=User',array("samaccountname")) or die('Error searching LDAP: '.ldap_error($mydap));
		$members = ldap_get_entries($mydap, $results);
		array_shift($members);
$cred = explode('\\',$_SERVER['REMOTE_USER']); 
if (count($cred) == 1) array_unshift($cred, "(no domain info - perhaps SSPIOmitDomain is On)"); 
list($domain, $user) = $cred; 
		foreach($members as $e){
		if(strtolower($e['samaccountname'][0]) == strtolower($user)){
			$output[] = $e['dn'];}
		}
		ldap_control_paged_result_response($mydap,$results,$counter);
	} while($counter !== null && $counter != "");
	return $output;
}
$user_resultd=array();
if(isset($_SERVER['REMOTE_USER']) && !empty($_SERVER['REMOTE_USER'])){
$cred = explode('\\',$_SERVER['REMOTE_USER']); 
if (count($cred) == 1) array_unshift($cred, "(no domain info - perhaps SSPIOmitDomain is On)"); 
list($domain, $user) = $cred; 

mydap_start(
	'John', // Active Directory search user
	'Temp1234', // Active Directory search user password
	'swkul1.local', // Active Directory server
	389 // Port (optional)
);

$data= "CN=".$user.",DC=SWKUL1,DC=local";
$data = "OU=User Directory,DC=SWKUL1,DC=local";
//$data = "CN=".$user.",OU=User Directory,DC=SWKUL1,DC=local";
//echo $data;
$members = mydap_members($data,'o');
if(!$members){
	$user_resultd["error"]= 100;
	$user_resultd["message"]= "NO MEMBER FOUND IN AD";
}else{
	$keep = array('samaccountname','mail','title','department','company','manager','homephone','whencreated');
	$i = 1;
	foreach($members as $m) {
		$attr = mydap_attributes($m,$keep);
		//$attr = mydap_attributes($m);
		$user_resultd["user"]["username"] = $samaccountname = isset($attr['samaccountname'][0]) ? $attr['samaccountname'][0] : "--";
		$user_resultd["user"]["mail"] = $mail = isset($attr['mail'][0]) ? $attr['mail'][0] : $attr['samaccountname'][0]."@ngc.com";
		$user_resultd["user"]["job"] = $job = isset($attr['title'][0]) ? $attr['title'][0] : "--";
		$user_resultd["user"]["department"] = $department = isset($attr['department'][0]) ? $attr['department'][0] : "--";
		$user_resultd["user"]["company"] = $company = isset($attr['company'][0]) ? $attr['company'][0] : "--";
		$user_resultd["user"]["homephone"] = $homephone = isset($attr['homephone'][0]) ? $attr['homephone'][0] : "--";
		$user_resultd["user"]["whencreated"] = $whencreated = isset($attr['whencreated'][0]) ? $attr['whencreated'][0] : "--";
		$manager = isset($attr['manager'][0]) ? $attr['manager'][0] : "--";
		if(isset($manager)){
		$m = explode(",",$manager)[0];
		$m =str_replace("CN=","",$m); 
		$user_resultd["user"]["manager"] = $m;
		}
		$date = DateTime::createFromFormat('Ymd', substr($whencreated,0,8));
		$user_resultd["user"]["whencreated"]= $date->format('Y-m-d');
		$i++;
	}
	mydap_end();
}
}else{
	$user_resultd["message"]= "NO LOGIN USER INFO";
}

?>