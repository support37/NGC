<html>
    <head>   
        <link href="assets/css/calendar.css" rel="stylesheet" type="text/css"/>
        <style>
            .tooltip_templates{
                display: none;
            }
        </style>
    </head>
    <body>
        <?php
        include 'calendar.php';

        $calendar = new Calendar();
        $calendar->prev = '&nbsp;';
        $calendar->next = '&nbsp;';
        $arrDays = array(25, 26, 29);
        $arrEvent = array(25 => '<div class="tooltip_templates">
                                <span id="tooltip_content2">
                                    <span class="tooltip-titile">Annual Dinner</span>
                                    <span class="tooltip-time">08:00pm</span>
                                </span>
                            </div>', 26 => '<div class="tooltip_templates">
                                <span id="tooltip_content2">
                                    <span class="tooltip-titile">Annual Dinner</span>
                                    <span class="tooltip-time">08:00pm</span>
                                </span>
                            </div>', 29 => '<div class="tooltip_templates">
                                <span id="tooltip_content2">
                                    <span class="tooltip-titile">Annual Dinner</span>
                                    <span class="tooltip-time">08:00pm</span>
                                </span>
                            </div>');
        echo $calendar->show($arrDays, $arrEvent);
        ?>
    </body>
</html>